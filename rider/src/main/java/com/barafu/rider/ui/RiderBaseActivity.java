package com.barafu.rider.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.events.BackgroundServiceStartedEvent;
import com.barafu.common.events.ConnectEvent;
import com.barafu.common.events.ConnectResultEvent;
import com.barafu.common.utils.MyPreferenceManager;
import com.barafu.rider.R;
import com.barafu.rider.services.RiderService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class RiderBaseActivity extends BaseActivity {
    MyPreferenceManager SP;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SP = MyPreferenceManager.getInstance(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isServiceRunning = isMyServiceRunning(RiderService.class);
        if (!isServiceRunning)
            startService(new Intent(this, RiderService.class));
    }
    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }
    public void tryConnect() {
        String token = SP.getString("rider_token", null);
        if (token != null && !token.isEmpty()) {
            eventBus.post(new ConnectEvent(token));
            if(connectionProgressDialog == null) {
                connectionProgressDialog = new MaterialDialog.Builder(this)
                        .title(getString(com.barafu.common.R.string.connection_dialog_title))
                        .content(R.string.event_reconnecting)
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
            } else {
                connectionProgressDialog.setContent(R.string.event_reconnecting);
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        if(connectionProgressDialog != null)
            connectionProgressDialog.dismiss();
    }
    public Bitmap resizeMarkerIcons(int resourceId){
        int width = 65;
        int height = 130;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(resourceId);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return smallMarker;
    }

}
