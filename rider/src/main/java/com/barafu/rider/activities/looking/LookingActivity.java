package com.barafu.rider.activities.looking;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.barafu.common.components.BaseActivity;
import com.barafu.common.models.Travel;
import com.barafu.rider.R;
import com.barafu.rider.databinding.ActivityLookingBinding;
import com.barafu.rider.events.CancelRequestRequestEvent;
import com.barafu.rider.events.DriverAcceptedEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LookingActivity extends BaseActivity {
    ActivityLookingBinding binding;
    Travel travel = new Travel();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setImmersive(true);
        binding = DataBindingUtil.setContentView(LookingActivity.this, R.layout.activity_looking);
        travel = Travel.fromJson(getIntent().getStringExtra("travel"));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDriverAccepted(DriverAcceptedEvent event) {
        binding.loadingIndicator.pauseAnimation();
        Intent intent = new Intent();
        Log.e("event.driverIDis",event.driver.getId()+"");
        intent.putExtra("driver", event.driver.toJson());
        intent.putExtra("driverId", event.driver.getId()+"");
        Log.e("event.driver.toJson",event.driver.toJson());
        this.setResult(RESULT_OK,intent);
        this.finish();

    }

    public void onCancelRequest(View view) {
        eventBus.post(new CancelRequestRequestEvent());
        this.setResult(RESULT_CANCELED);
        this.finish();
    }
}
