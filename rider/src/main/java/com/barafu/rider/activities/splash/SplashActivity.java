package com.barafu.rider.activities.splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.databinding.DataBindingUtil;

import com.barafu.common.MyTaxiApplication;
import com.barafu.common.activities.login.LoginActivity;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.events.BackgroundServiceStartedEvent;
import com.barafu.common.events.ConnectEvent;
import com.barafu.common.events.ConnectResultEvent;
import com.barafu.common.events.LoginEvent;
import com.barafu.common.models.Rider;
import com.barafu.common.utils.AlertDialogBuilder;
import com.barafu.common.utils.AlerterHelper;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.ConnectivityReceiver;
import com.barafu.common.utils.LocationHelper;
import com.barafu.common.utils.MyPreferenceManager;
import com.barafu.rider.BuildConfig;
import com.barafu.rider.R;
import com.barafu.rider.RiderEventBusIndex;
import com.barafu.rider.activities.main.MainActivity;
import com.barafu.rider.activities.update_version.VersionUpdateActivity;
import com.barafu.rider.databinding.ActivitySplashBinding;
import com.barafu.rider.events.LoginResultEvent;
import com.barafu.rider.services.RiderService;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity implements LocationListener {
    MyPreferenceManager SP;
    ActivitySplashBinding binding;
    int RC_SIGN_IN = 123;
    Handler locationTimeoutHandler;
    LocationManager locationManager;
    LatLng currentLocation;
    boolean isErrored = false;
    boolean goingToOpen = false;

    private PermissionListener permissionlistener = new PermissionListener() {
        @SuppressLint("MissingPermission")
        @Override
        public void onPermissionGranted() {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(SplashActivity.this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(SplashActivity.this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        }
                    });
            boolean isServiceRunning = isMyServiceRunning(RiderService.class);
            if (!isServiceRunning)
                startService(new Intent(SplashActivity.this, RiderService.class));
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            boolean isServiceRunning = isMyServiceRunning(RiderService.class);
            if (!isServiceRunning)
                startService(new Intent(SplashActivity.this, RiderService.class));
        }
    };
    private View.OnClickListener onLoginButtonClicked = v -> {
        String resourceName = "testMode";
        int testExists = SplashActivity.this.getResources().getIdentifier(resourceName, "string", SplashActivity.this.getPackageName());
        if (testExists > 0) {
            tryLogin(getString(testExists));
            return;
        }
        if (getResources().getBoolean(R.bool.use_custom_login)) {
            startActivityForResult(new Intent(SplashActivity.this, LoginActivity.class), RC_SIGN_IN);
        } else
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .setTheme(getCurrentTheme())
                            .build(),
                    RC_SIGN_IN);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setImmersive(true);
        showConnectionDialog = false;
        try {
            EventBus.builder().addIndex(new RiderEventBusIndex()).installDefaultEventBus();
        } catch (Exception ignored) {
        }
        super.onCreate(savedInstanceState);
        // Initialize Places.
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

    // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (!getString(R.string.fabric_key).equals("")) {
            Fabric.with(this, new Crashlytics());
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(SplashActivity.this, R.layout.activity_splash);

        setValues();
        showNetworkAlertDialog();
    }

    private void setValues() {
        binding.loginButton.setOnClickListener(onLoginButtonClicked);
        SP = MyPreferenceManager.getInstance(getApplicationContext());
        checkPermissions();


        setLanguageSpinner();
        setCurrencyAdapter();

    }
    private void showNetworkAlertDialog() {
        if (!ConnectivityReceiver.isConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage("There seems to be an issue conecting to internet.\nPlease make sure you're connected to the internet.");
            builder.setPositiveButton("OK", (dialog, id) -> {
                dialog.dismiss();
                if (ConnectivityReceiver.isConnected()) {
                    showNetworkAlertDialog();
                }
            });
            builder.setNegativeButton("NO", (dialog, id) -> {
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        } else
            checkVersion();
    }
    private void checkVersion() {
        String version_name = com.barafu.common.BuildConfig.VERSION_NAME;

        VersionChecker versionChecker = new VersionChecker();
        try {
            String latestVersion = versionChecker.execute().get();
            Log.e("latestVersion", latestVersion + " ---- " + version_name);
            if (version_name.equals(latestVersion)) {
                setValues();
            } else {
                if (getUpdateCount() == 1) {
                    Log.e("ShowingCount", getUpdateCount() + " ShowingCount");
                    startActivity(new Intent(this, VersionUpdateActivity.class));
                    finish();

                } else if (getUpdateCount() == 5) {
                    setUpdateCount(0);
                    setValues();
                }
                else
                    setValues();

            }
            setUpdateCount(getUpdateCount() + 1);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
    public class VersionChecker extends AsyncTask<String, String, String> {

        String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

    }

    int currencyCount = 0;

    private void setCurrencyAdapter() {
        Spinner language = findViewById(R.id.spCurrency);
        List<String> list = new ArrayList<>();
//        list.add(getString(R.string.usd));
        list.add(getString(R.string.tzs));
//        list.add(getString(R.string.ksh));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        language.setAdapter(dataAdapter);

        if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.usd)))
            language.setSelection(0);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.tzs)))
            language.setSelection(1);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.ksh)))
            language.setSelection(2);

        language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (++currencyCount > 1) {
                    if (selectedItem.equals(getString(R.string.usd))) {
                        MyTaxiApplication.setCurrency(getString(R.string.usd));
                        showCurrencyChangedDialog();
                    } else if (selectedItem.equals(getString(R.string.tzs))) {
                        MyTaxiApplication.setCurrency(getString(R.string.tzs));
                        showCurrencyChangedDialog();
                    } else if (selectedItem.equals(getString(R.string.ksh))) {
                        MyTaxiApplication.setCurrency(getString(R.string.ksh));
                        showCurrencyChangedDialog();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    int count = 0;

    public void setLanguageSpinner() {
        Spinner language = findViewById(R.id.spLanguage);
        List<String> list = new ArrayList<>();
        list.add(getString(R.string.english));
        list.add(getString(R.string.spanish));
//        list.add(getString(R.string.french));
//        list.add(getString(R.string.swahili));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language.setAdapter(dataAdapter);
        if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.english)))
            language.setSelection(0);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.spanish)))
            language.setSelection(1);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.french)))
            language.setSelection(2);

        language.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (++count > 1) {
                    if (selectedItem.equals(getString(R.string.english))) {
                        setLanguage("en_US");
                        setLanguageCurrent(getString(R.string.english));
                        showLanguagechangedMessage();
                    } else if (selectedItem.equals(getString(R.string.spanish))) {
                        setLanguage("es");
                        setLanguageCurrent(getString(R.string.spanish));
                        showLanguagechangedMessage();
                    } else if (selectedItem.equals(getString(R.string.french))) {
                        setLanguage("fr");
                        setLanguageCurrent(getString(R.string.french));
                        showLanguagechangedMessage();
                    }
//                    else if (selectedItem.equals(getString(R.string.swahili))) {
//                        setLanguage("sw");
//                        setLanguageCurrent(getString(R.string.swahili));
//                        showLanguagechangedMessage();
//                    }
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showLanguagechangedMessage() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Change Language")
                .setMessage("Language changed Successfully")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                })
                .show();

//        TextView textView = alertDialog.findViewById(android.R.id.message);
//        textView.setTextColor(ContextCompat.getColor(this, R.color.accent_white));

    }
    public void showCurrencyChangedDialog() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Currency")
                .setMessage("Currency changed Successfully")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();

//        TextView textView = alertDialog.findViewById(android.R.id.message);
//        textView.setTextColor(ContextCompat.getColor(this, R.color.accent_white));

    }

    private void checkPermissions() {
        if (CommonUtils.isInternetDisabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_enable_wifi), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    @SuppressLint("MissingPermission")
    private void searchCurrentLocation() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 1, this);
    }

    private void startMainActivity(LatLng latLng) {
        if (goingToOpen)
            return;
        goingToOpen = true;
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        double[] array = LocationHelper.LatLngToDoubleArray(latLng);
        intent.putExtra("currentLocation", array);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginResultEvent(LoginResultEvent event) {
        if (event.hasError()) {
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    binding.loginButton.callOnClick();
                else
                    finish();
            });
            return;
        }
        CommonUtils.rider = event.rider;
        SP.putString("rider_user", event.riderJson);
        SP.putString("rider_token", event.jwtToken);
        tryConnect();
    }

    public void tryConnect() {
        String token = SP.getString("rider_token", null);
        if (token != null && !token.isEmpty()) {
            eventBus.post(new ConnectEvent(token));
            goToLoadingMode();
        } else {
            goToLoginMode();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {

        if (event.hasError()) {
            isErrored = true;
            binding.progressBar.setVisibility(View.GONE);
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    tryConnect();
                } else {
                    goToLoginMode();
                }
            });
            return;
        }
        locationTimeoutHandler = new Handler();
        locationTimeoutHandler.postDelayed(() -> {
            locationManager.removeUpdates(SplashActivity.this);
            if (currentLocation == null) {
                String[] location = getString(R.string.defaultLocation).split(",");
                double lat = Double.parseDouble(location[0]);
                double lng = Double.parseDouble(location[1]);
                currentLocation = new LatLng(lat, lng);
            }
            if (isErrored)
                return;
            startMainActivity(currentLocation);

        }, 5000);
        searchCurrentLocation();
        CommonUtils.rider = Rider.fromJson(SP.getString("rider_user", "{}"));
    }

    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tryConnect();
    }

    private void tryLogin(String phone) {
        isErrored = false;
        goToLoadingMode();
        if (phone.substring(0, 1).equals("+"))
            phone = phone.substring(1);
        eventBus.post(new LoginEvent(Long.valueOf(phone), BuildConfig.VERSION_CODE));
    }

    private void goToLoadingMode() {
        binding.llLanguageLayout.setVisibility(View.GONE);
        binding.loginButton.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void goToLoginMode() {
        binding.loginButton.setVisibility(View.VISIBLE);
        binding.llLanguageLayout.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                if (getResources().getBoolean(R.bool.use_custom_login)) {
                    tryLogin(data.getStringExtra("mobile"));
                } else {
                    IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                    String phone;
                    if (idpResponse != null) {
                        phone = idpResponse.getPhoneNumber();
                        tryLogin(phone);
                        return;
                    }
                }
                return;
            }
        }
        AlerterHelper.showError(SplashActivity.this, getString(R.string.login_failed));
        goToLoginMode();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
