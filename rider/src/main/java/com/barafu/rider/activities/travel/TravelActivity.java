package com.barafu.rider.activities.travel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.transition.TransitionManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.barafu.common.MyTaxiApplication;
import com.barafu.common.activities.chargeAccount.ChargeAccountActivity;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.components.LoadingDialog;
import com.barafu.common.events.ServiceCallRequestEvent;
import com.barafu.common.events.ServiceCallRequestResultEvent;
import com.barafu.common.events.ServiceCancelEvent;
import com.barafu.common.events.ServiceCancelResultEvent;
import com.barafu.common.location.MapHelper;
import com.barafu.common.models.Coupon;
import com.barafu.common.models.Driver;
import com.barafu.common.models.Review;
import com.barafu.common.models.Travel;
import com.barafu.common.utils.AlertDialogBuilder;
import com.barafu.common.utils.AlerterHelper;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.MapDirectionsParser;
import com.barafu.common.utils.ServerResponse;
import com.barafu.rider.R;
import com.barafu.rider.activities.coupon.CouponActivity;
import com.barafu.rider.activities.travel.adapters.TravelTabsViewPagerAdapter;
import com.barafu.rider.activities.travel.fragments.ReviewDialog;
import com.barafu.rider.activities.travel.fragments.TabStatisticsFragment;
import com.barafu.rider.databinding.ActivityTravelBinding;
import com.barafu.rider.events.ReviewDriverEvent;
import com.barafu.rider.events.ReviewDriverResultEvent;
import com.barafu.rider.events.ServiceFinishedEvent;
import com.barafu.rider.events.ServiceStartedEvent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.tabs.TabLayout;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TravelActivity extends BaseActivity implements OnMapReadyCallback, ReviewDialog.onReviewFragmentInteractionListener, TabStatisticsFragment.onTravelInfoReceived {
    private static final int ACTIVITY_COUPON = 700;
    ActivityTravelBinding binding;
    Travel travel;
    Marker pickupMarker;
    Marker driverMarker;
    Marker destinationMarker;
    LatLng driverLocation;
    GoogleMap gMap;
    TravelTabsViewPagerAdapter travelTabsViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_travel);
        travel = Travel.fromJson(getIntent().getStringExtra("travel"));
        Log.e("travelDriverDataOne", Travel.fromJson(getIntent().getStringExtra("travel"))+"");
        Log.e("travelDriverData",getIntent().getStringExtra("travel")+"");
        binding.slideCancel.setOnSlideCompleteListener(slideView -> eventBus.post(new ServiceCancelEvent()));
        binding.slideCall.setOnSlideCompleteListener(slideView -> TravelActivity.this.onCallDriverClicked(null));
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        travelTabsViewPagerAdapter = new TravelTabsViewPagerAdapter(getSupportFragmentManager(), TravelActivity.this, travel);
        binding.viewpager.setAdapter(travelTabsViewPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewpager);
        if (travel.getRating() != null) {
            travelTabsViewPagerAdapter.deletePage(2);
            TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            if (tab != null)
                tab.select();
        }
        if (travel.getStartTimestamp() != null) {
            TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
            binding.slideCall.setVisibility(View.GONE);
            binding.slideCancel.setVisibility(View.GONE);
        }
        getDriverInformation();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceFinished(ServiceFinishedEvent event) {
        String message;
        travel.setFinishTimestamp(new Timestamp(System.currentTimeMillis()));
        if (event.isCreditUsed)
            message = getString(R.string.travel_finished_taken_from_balance, event.amount);
        else
            message = getString(R.string.travel_finished_not_sufficient_balance, event.amount);
        new MaterialDialog.Builder(this)
                .title(R.string.message_default_title)
                .content(message)
                .positiveText(R.string.alert_ok)
                .onPositive((dialog, which) -> {
                    if (travelTabsViewPagerAdapter.getCount() == 2) {
                        finish();
                        return;
                    }
                    FragmentManager fm = getSupportFragmentManager();
                    ReviewDialog reviewDialog = ReviewDialog.newInstance();
                    reviewDialog.show(fm, "fragment_review_travel");
                }).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceCanceled(ServiceCancelResultEvent event) {
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    eventBus.post(new ServiceCancelEvent());
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.service_canceled), AlertDialogBuilder.DialogButton.OK, result -> finish());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReviewResult(ReviewDriverResultEvent event) {
        if (event.response == ServerResponse.OK) {
            if (travel.getFinishTimestamp() != null) {
                finish();
                return;
            }
            AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_review_sent));
            travelTabsViewPagerAdapter.deletePage(2);
            TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            if (tab != null)
                tab.select();

        } else
            event.showAlert(TravelActivity.this);
    }

    public void onChargeAccountClicked(View view) {
        Intent intent = new Intent(TravelActivity.this, ChargeAccountActivity.class);
        if (travel.getCostBest() - CommonUtils.rider.getBalance() > 0)
            intent.putExtra("defaultAmount", travel.getCostBest() - CommonUtils.rider.getBalance());
        startActivity(intent);
    }

    public void onApplyCouponClicked(View view) {
        Intent intent = new Intent(TravelActivity.this, CouponActivity.class);
        intent.putExtra("select_mode", true);
        startActivityForResult(intent, ACTIVITY_COUPON);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTravelStarted(ServiceStartedEvent event) {
        travel.setStartTimestamp(new Timestamp(System.currentTimeMillis()));
        AlerterHelper.showInfo(TravelActivity.this, getString(R.string.message_travel_started));
        updateMarkers();
        TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
        binding.slideCall.setVisibility(View.GONE);
        binding.slideCancel.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setTrafficEnabled(true);
        updateMarkers();
    }

    void updateMarkers() {
        List<LatLng> locations = new ArrayList<>();
        if (pickupMarker == null)
            pickupMarker = gMap.addMarker(new MarkerOptions()
                    .position(travel.getPickupPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_pickup)));
        else
            pickupMarker.setPosition(travel.getPickupPoint());
        if (destinationMarker == null)
            destinationMarker = gMap.addMarker(new MarkerOptions()
                    .position(travel.getDestinationPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_destination)));
        else
            destinationMarker.setPosition(travel.getDestinationPoint());
        if (driverLocation != null) {
            locations.add(driverLocation);
            if (travel.getStartTimestamp() != null)
                locations.add(travel.getDestinationPoint());
            else
                locations.add(travel.getPickupPoint());
            if (driverMarker == null) {
                driverMarker = gMap.addMarker(new MarkerOptions()
                        .position(driverLocation)
                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMarkerIcons(R.drawable.pin_driver))).flat(true));
            } else {
                float bearing = (float) bearingBetweenLocations(driverMarker.getPosition(), driverLocation);
                rotateMarker(driverMarker, bearing);

                driverMarker.setPosition(driverLocation);
            }
        } else {
            locations.add(travel.getPickupPoint());
            locations.add(travel.getDestinationPoint());
        }
        boolean allEqual = true;
        for (int i = 0; i < locations.size() - 1; i++) {
            if (locations.get(i).latitude != locations.get(i + 1).latitude || locations.get(i).longitude != locations.get(i + 1).longitude) {
                allEqual = false;
                break;
            }
        }
        if (!allEqual)
            MapHelper.centerLatLngsInMap(gMap, locations, true);
        else {
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations.get(0), 18));
        }
        traceMe(pickupMarker.getPosition(),destinationMarker.getPosition());

    }

    boolean isMarkerRotating;

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 2000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    float bearing = -rot > 180 ? rot / 2 : rot;

                    marker.setRotation(bearing);

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    PermissionListener callPermissionListener = new PermissionListener() {
        @SuppressLint("MissingPermission")
        @Override
        public void onPermissionGranted() {
            Log.e("mobileNumberIs",travel.getDriver().getMobileNumber()+"");
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:+" + travel.getDriver().getMobileNumber()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {

        }

    };


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCallRequested(ServiceCallRequestResultEvent event) {
        LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    onCallDriverClicked(null);
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.call_request_sent));
    }

    @Override
    public void onReviewTravelClicked(Review review) {
        eventBus.post(new ReviewDriverEvent(review));
    }

    public void onCallDriverClicked(View view) {
        boolean isCallRequestEnabled = getResources().getBoolean(R.bool.is_call_request_enabled_rider);
        boolean isDirectCallEnabled = getResources().getBoolean(R.bool.is_direct_call_enabled_rider);
        if (isCallRequestEnabled && !isDirectCallEnabled)
            eventBus.post(new ServiceCallRequestEvent());
        if (!isCallRequestEnabled && isDirectCallEnabled)
            TedPermission.with(this)
                    .setPermissionListener(callPermissionListener)
                    .setDeniedMessage(R.string.message_permission_denied)
                    .setPermissions(Manifest.permission.CALL_PHONE)
                    .check();
        new MaterialDialog.Builder(this)
                .title(R.string.select_contact_approach)
                .items(new String[]{getString(R.string.direct_call), getString(R.string.operator_call)})
                .itemsCallback((dialog, view1, which, text) -> {
                    if (which == 0)
                        TedPermission.with(TravelActivity.this)
                                .setPermissionListener(callPermissionListener)
                                .setDeniedMessage(R.string.message_permission_denied)
                                .setPermissions(Manifest.permission.CALL_PHONE)
                                .check();
                    if (which == 1)
                        eventBus.post(new ServiceCallRequestEvent());
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_COUPON) {
            if (resultCode == RESULT_OK) {
                Coupon coupon = (Coupon) data.getSerializableExtra("coupon");
                String message = "";
                if (coupon.getFlatDiscount() == 0 && coupon.getDiscountPercent() != 0)
                    message = "Coupon with discount of " + coupon.getDiscountPercent() + "% has been applied.";
                if (coupon.getFlatDiscount() != 0 && coupon.getDiscountPercent() == 0)
                    message = "Coupon with discount of " + MyTaxiApplication.getCurrency()+" "+ coupon.getFlatDiscount() + " has been applied.";
//                    message = "Coupon with discount of " + getString(R.string.unit_money, coupon.getFlatDiscount()) + " has been applied.";
                if (coupon.getFlatDiscount() != 0 && coupon.getDiscountPercent() != 0)
                    message = "Coupon with discount of " +MyTaxiApplication.getCurrency()+" "+ coupon.getFlatDiscount() + " and " + coupon.getDiscountPercent() + "% has been applied.";
//                    message = "Coupon with discount of " + getString(R.string.unit_money, coupon.getFlatDiscount()) + " and " + coupon.getDiscountPercent() + "% has been applied.";
                if (message.equals(""))
                    return;
                AlerterHelper.showInfo(TravelActivity.this, message);
                travelTabsViewPagerAdapter.statisticsFragment.onUpdatePrice(data.getDoubleExtra("costAfterCoupon", travel.getCostBest()));

            }
        }
    }

    @Override
    public void onReceived(LatLng driverLocation, float cost) {
        this.driverLocation = driverLocation;
        updateMarkers();
    }

//    ProgressDialog PD;
    private ArrayList<LatLng> traceOfMe = null;
    private Polyline mPolyline = null;

    private void traceMe(LatLng srcLatLng, LatLng destLatLng) {
//        Log.e("traceMeCalled","traceMeCalled");
        {
//            PD = new ProgressDialog(TravelActivity.this);
//            PD.setMessage("Loading..");
//            PD.show();

            String srcParam = srcLatLng.latitude + "," + srcLatLng.longitude;
            String destParam = destLatLng.latitude + "," + destLatLng.longitude;

            String modes[] = {"driving", "walking", "bicycling", "transit"};

            String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=AIzaSyBcyeuQMy-WBeywimvuRhg7Pvil0IJXYmM";
//            Log.e("url",url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response1 -> {
                        try {

                            Log.e("responseGoogle",response1);
                            JSONObject response = null;

                            response = new JSONObject(response1);


                            MapDirectionsParser parser = new MapDirectionsParser();
                            List<List<HashMap<String, String>>> routes = parser.parse(response);
                            ArrayList<LatLng> points = null;

                            for (int i = 0; i < routes.size(); i++) {
                                points = new ArrayList<LatLng>();
                                // lineOptions = new PolylineOptions();

                                // Fetching i-th route
                                List<HashMap<String, String>> path = routes.get(i);

                                // Fetching all the points in i-th route
                                for (int j = 0; j < path.size(); j++) {
                                    HashMap<String, String> point = path.get(j);

                                    double lat = Double.parseDouble(point.get("lat"));
                                    double lng = Double.parseDouble(point.get("lng"));
                                    LatLng position = new LatLng(lat, lng);

                                    points.add(position);
                                }
                            }

                            drawPoints(points, gMap);
//                            PD.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    },
                    error -> {}) {

            };


            RequestQueue requestQueue = Volley.newRequestQueue(TravelActivity.this);
            requestQueue.add(stringRequest);

            stringRequest.setRetryPolicy(new com.android.volley.DefaultRetryPolicy(
                    10000,
                    com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }
    }

    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        traceOfMe = points;
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : traceOfMe) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (gMap != null) {
            mPolyline = gMap.addPolyline(polylineOpt);

        } else {

        }
        if (mPolyline != null)
            mPolyline.setWidth(10);
        if (gMap != null) {

            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupMarker.getPosition(), 18.0f));

        }
    }
    private void getDriverInformation() {
        String url = "http://3.130.44.140/app/api/getdriverInfo/"+travel.getDriver().getId() ;
        Log.e("url",url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,

                response -> {

                    Log.e("response", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String result_code = jsonObject.getString("result_code");
                        if (result_code.equalsIgnoreCase("200")) {
                            JSONObject riderObj = new JSONObject(jsonObject.getString("driver_info"));
                            String mobile_number = riderObj.getString("mobile_number");
                            String first_name = riderObj.getString("first_name");
                            String last_name = riderObj.getString("last_name");
                            Log.e("mobile_number", mobile_number);
                            try {
                                Driver rider = new Driver();
                                long mobile = Long.valueOf(mobile_number);
                                rider.setMobileNumber(mobile);
                                rider.setFirstName(first_name);
                                rider.setLastName(last_name);
                                travel.setDriver(rider);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Toast.makeText(this, "Network Error. Please try again after sometime.", Toast.LENGTH_LONG).show();
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(TravelActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                40000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


    }

}
