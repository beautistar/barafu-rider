package com.barafu.rider.activities.travel.fragments;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.barafu.common.components.BaseFragment;
import com.barafu.common.models.Review;
import com.barafu.common.models.Travel;
import com.barafu.rider.R;
import com.barafu.rider.databinding.FragmentTravelReviewBinding;
import com.barafu.rider.events.ReviewDriverEvent;

import org.greenrobot.eventbus.EventBus;

public class TabReviewFragment extends BaseFragment {
    FragmentTravelReviewBinding binding;

    TextView tvDriverCount;
    RatingBar rvDriverRating;
    Travel travel;

    public TabReviewFragment(Travel travel) {
        this.travel = travel;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = EventBus.getDefault();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_travel_review, container, false);


        tvDriverCount = binding.getRoot().findViewById(R.id.tvDriverCount);
        rvDriverRating = binding.getRoot().findViewById(R.id.rvDriverRating);
        if (travel != null) {
            String reviewCount = travel.getDriver().getReviewCount() + " Reviews";
            tvDriverCount.setText(reviewCount);
            if(travel.getDriver().getRating()!=null)
            {
                 rvDriverRating.setRating(travel.getDriver().getRating());
            }
        }
        binding.ratingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> binding.buttonSaveReview.setEnabled(true));
        binding.buttonSaveReview.setOnClickListener(view -> {
            Review review = new Review((int) binding.ratingBar.getRating() * 20, binding.reviewText.getText().toString());
            eventBus.post(new ReviewDriverEvent(review));
        });
        return binding.getRoot();
    }
}
