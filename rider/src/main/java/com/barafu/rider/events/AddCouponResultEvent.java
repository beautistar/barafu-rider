package com.barafu.rider.events;

import com.barafu.common.events.BaseResultEvent;
import com.barafu.common.utils.ServerResponse;

public class AddCouponResultEvent extends BaseResultEvent {
    public AddCouponResultEvent(){
        super(ServerResponse.REQUEST_TIMEOUT);
    }
    public AddCouponResultEvent(Object... args) {
        super(args);
    }
}
