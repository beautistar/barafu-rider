package com.barafu.rider.events;

import com.barafu.common.events.BaseResultEvent;
import com.barafu.common.utils.ServerResponse;

public class ApplyCouponResultEvent extends BaseResultEvent {
    public double finalPrice;
    public ApplyCouponResultEvent(){
        super(ServerResponse.REQUEST_TIMEOUT);
    }
    public ApplyCouponResultEvent(Object... args) {
        super(args);
        if(hasError())
            return;
        this.finalPrice = (double)args[1];
    }
}
