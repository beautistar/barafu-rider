package com.barafu.rider.events;

import com.barafu.common.events.BaseRequestEvent;

public class GetCouponsRequestEvent extends BaseRequestEvent {
    public GetCouponsRequestEvent() {
        super(new GetCouponsResultEvent());
    }
}
