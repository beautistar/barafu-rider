package com.barafu.rider.events;

import com.barafu.common.events.BaseRequestEvent;
import com.barafu.common.models.Review;
import com.barafu.common.utils.ServerResponse;

public class ReviewDriverEvent extends BaseRequestEvent {
    public Review review;
    public ReviewDriverEvent(Review review){
        super(new ReviewDriverResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
        this.review = review;
    }
}
