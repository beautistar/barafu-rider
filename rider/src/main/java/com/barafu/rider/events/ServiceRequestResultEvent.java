package com.barafu.rider.events;

import com.barafu.common.events.BaseResultEvent;

public class ServiceRequestResultEvent extends BaseResultEvent {
    public int driversSentTo;
    public ServiceRequestResultEvent(int driversSentTo) {
        super(200);
        this.driversSentTo = driversSentTo;
    }
}
