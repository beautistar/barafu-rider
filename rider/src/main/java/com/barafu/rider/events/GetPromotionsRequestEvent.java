package com.barafu.rider.events;

import com.barafu.common.events.BaseRequestEvent;

public class GetPromotionsRequestEvent extends BaseRequestEvent {
    public GetPromotionsRequestEvent() {
        super(new GetPromotionsResultEvent());
    }
}
