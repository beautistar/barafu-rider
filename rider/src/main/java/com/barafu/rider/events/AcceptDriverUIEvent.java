package com.barafu.rider.events;

import com.barafu.common.models.DriverInfo;

public class AcceptDriverUIEvent {
    public DriverInfo driverInfo;
    public AcceptDriverUIEvent(DriverInfo driverInfo){
        this.driverInfo = driverInfo;
    }
}
