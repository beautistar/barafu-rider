package com.barafu.rider.events;

import com.google.gson.Gson;
import com.barafu.common.models.Driver;
import com.barafu.common.models.DriverInfo;

public class DriverAcceptedEvent {
    public Driver driver;
    public DriverAcceptedEvent(Object... args) {
        Gson gson = new Gson();
        driver = gson.fromJson(args[0].toString(), Driver.class);
    }
}
