package com.barafu.rider.events;

import com.barafu.common.events.BaseResultEvent;

public class ReviewDriverResultEvent extends BaseResultEvent {
    public ReviewDriverResultEvent(int response) {
        super(response);
    }
}
