package com.barafu.rider.events;

import com.barafu.common.events.BaseRequestEvent;

public class ApplyCouponRequestEvent extends BaseRequestEvent {
    public String code;
    public ApplyCouponRequestEvent(String code) {
        super(new ApplyCouponResultEvent());
        this.code = code;
    }
}
