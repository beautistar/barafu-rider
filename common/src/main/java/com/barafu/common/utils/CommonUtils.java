package com.barafu.common.utils;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.barafu.common.models.Driver;
import com.barafu.common.models.Rider;

import java.text.DecimalFormat;

public class CommonUtils {
    public static Driver driver;
    public static Rider rider;
    public final static int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    public static CountDownTimer currentTimer;
    public static LatLng currentLocation;
    public static final String MY_PREFS_NAME = "barafu_pref";
//    public static final String ENGLISH = "english";
//    public static final String SPANISH = "spanish";

    public static final String MY_PREFS_DRIVER = "barafu_driver_pref";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String USER_NAME = "user_name";

    public final static int CONTACT_INTENT = 10010;
    public final static int READ_CONTACT_PERMISION = 10011;

    public static boolean isInternetDisabled(AppCompatActivity activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return true;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return !haveConnectedWifi && !haveConnectedMobile;
    }

    public static boolean isGPSEnabled(AppCompatActivity activity) {
        LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static String removeDecimal(double travelCost) {
        DecimalFormat f = new DecimalFormat("##");
        return f.format(travelCost);
    }

    public static String addSaperator(String str) {
        if (str.length() > 3) {
            Log.e("lengthISsss", str.length() + "");
            str = str.substring(0, str.length() - 3) + "," + str.substring(str.length() - 3, str.length());
        }
        if (str.length() > 7) {
            str = str.substring(0, str.length() - 7) + "," + str.substring(str.length() - 7, str.length());
        }

        return str;
    }

}