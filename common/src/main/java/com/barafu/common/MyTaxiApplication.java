package com.barafu.common;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.multidex.MultiDex;
import androidx.appcompat.app.AppCompatDelegate;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.barafu.common.utils.CommonUtils;
import com.google.firebase.FirebaseApp;
import com.onesignal.OneSignal;

// Just for test
public class MyTaxiApplication extends Application {
    private static MyTaxiApplication mInstance;

    @Override
    public void onCreate() {
        FirebaseApp.initializeApp(getApplicationContext());
        int nightMode = AppCompatDelegate.MODE_NIGHT_NO;
        AppCompatDelegate.setDefaultNightMode(nightMode);
        mInstance = this;
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
    //************* set Default Language*****************
    public static void setCurrency(String count) {
        SharedPreferences.Editor editor = mInstance.getSharedPreferences(CommonUtils.MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("selected_currency", count);
        editor.apply();
    }

    //************* get Default Language*****************
    public static String getCurrency() {
        SharedPreferences prefs = mInstance.getSharedPreferences(CommonUtils.MY_PREFS_NAME, MODE_PRIVATE);
        return prefs.getString("selected_currency", mInstance.getString(R.string.tzs));
    }

    @Override
    protected void attachBaseContext(Context base) {
        /*LocaleHelper.setLocale(base,"en");*/
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static void setmRequestQueue(StringRequest stringRequest) {
        RequestQueue requestQueue = Volley.newRequestQueue(mInstance);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                40000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }
    public static synchronized MyTaxiApplication getInstance() {
        return mInstance;
    }

}