package com.barafu.common.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.barafu.common.R;
import com.barafu.common.models.AddressModel;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

public class AddressAutoCompleteAdapter extends ArrayAdapter<AddressModel> {
    private ArrayList<AddressModel> tempCustomer;
    private ArrayList<AddressModel> suggestions;


    public AddressAutoCompleteAdapter(Context context, ArrayList<AddressModel> objects) {
        super(context, R.layout.address_layout, objects);
        this.tempCustomer = new ArrayList<>(objects);
        this.suggestions = new ArrayList<>(objects);

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        AddressModel customer = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.address_layout, parent, false);
        }

        TextView txtCustomer = (TextView) convertView.findViewById(R.id.tvAddressAC);
        if (txtCustomer != null)
            txtCustomer.setText(customer.getAddress());


      /*  convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ChIJTUbDjDsYAHwRbJen81_1KEs
                activity.getLocDetails(getItem(position).getPlaceID());
            }
        });*/

        return convertView;
    }


    @NonNull
    @Override
    public Filter getFilter() {
        return myFilter;
    }

    private Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            AddressModel customer = (AddressModel) resultValue;
            return customer.getAddress();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (AddressModel people : tempCustomer) {
                    if (people.getAddress().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                ArrayList<AddressModel> c = (ArrayList<AddressModel>) results.values;
                if (results.count > 0) {
                    clear();
                    for (AddressModel cust : c) {
                        add(cust);
                        notifyDataSetChanged();
                    }
                }
            }catch (ConcurrentModificationException e)
            {e.printStackTrace();}
        }
    };
}
