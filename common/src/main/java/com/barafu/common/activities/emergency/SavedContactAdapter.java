package com.barafu.common.activities.emergency;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.barafu.common.R;
import com.barafu.common.activities.emergency.contact_list.ContactListModel;

import java.util.List;


public class SavedContactAdapter extends RecyclerView.Adapter<SavedContactAdapter.MyViewHolder> {
    private Context context;
    private List<ContactListModel> contactListArray;
    EmergencyActivity emergencyActivity;

    public SavedContactAdapter(List<ContactListModel> storedFileArray, Context context) {
        this.contactListArray = storedFileArray;
        this.context = context;
        emergencyActivity = (EmergencyActivity) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_saved_contact, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        holder.tvContactName.setText(contactListArray.get(position).getName());
        holder.tvContactNumber.setText(contactListArray.get(position).getPhoneNum());
        holder.ivCallUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emergencyActivity.onCallClicked(contactListArray.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return contactListArray.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvContactName;
        ImageView ivCallUser;
        TextView tvContactNumber;

        public MyViewHolder(View view) {
            super(view);

            tvContactName = view.findViewById(R.id.tvContactNameEm);
            tvContactNumber = view.findViewById(R.id.tvContactNumberEm);
            ivCallUser = view.findViewById(R.id.ivCallUser);

        }
    }


}
