package com.barafu.common.activities.emergency.contact_list;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barafu.common.R;
import com.barafu.common.activities.emergency.contact_list.adapter.ContactUsAdapter;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public class ContactListActivity extends BaseActivity {

    ProgressBar pbContactList;

    List<ContactListModel> contactListTemp;
    List<ContactListModel> contactList;
    List<ContactListModel> searchArray;
    RecyclerView rvContactList;
    String lastNumber = "";
    SearchView searchView;
    MenuItem searchItem;
    ContactUsAdapter contactUsAdapter;
    boolean isSearchEnabled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

//        ButterKnife.bind(this);
        initializeToolbar("Contact List");
        initViews();

        rvContactList.addOnItemTouchListener(

                new RecyclerItemClickListener(this, (view, position) -> {
                    if (isSearchEnabled) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(CommonUtils.PHONE_NUMBER, searchArray.get(position).getPhoneNum());
                        returnIntent.putExtra(CommonUtils.USER_NAME, searchArray.get(position).getName());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(CommonUtils.PHONE_NUMBER, contactList.get(position).getPhoneNum());
                        returnIntent.putExtra(CommonUtils.USER_NAME, contactList.get(position).getName());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                })
        );

        getContacts();

    }

    private void getContacts() {
        pbContactList.setVisibility(View.VISIBLE);
        try {
            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            while (Objects.requireNonNull(phones).moveToNext()) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                String phoneNumberIs = phoneNumber.replace(" ", "").trim().replace("+", "");
                String detectNumber91 = phoneNumberIs.substring(0, 2);
                String detectNumber0 = phoneNumberIs.substring(0, 1);

                String newString;
                if (detectNumber0.equals("0"))
                    newString = phoneNumberIs.substring(1);
                else if (detectNumber91.equals("91"))
                    newString = phoneNumberIs.substring(2);
                else
                    newString = phoneNumberIs;


                if (!newString.trim().equals(lastNumber.trim())) {
                    if (newString.matches("^[0-9]+$")) // returns true
                        contactListTemp.add(new ContactListModel(name, newString.replace("+", "")));
                }
                lastNumber = newString;
            }
            phones.close();

            if (contactListTemp.size() > 0) {
                Collections.sort(contactListTemp, (o1, o2) -> o1.getName().compareTo(o2.getName()));
                String preNum = "";

                for (int i = 0; i < contactListTemp.size(); i++) {
                    String curPhone = contactListTemp.get(i).getPhoneNum();
                    if (!curPhone.equals(preNum))
                        contactList.add(contactListTemp.get(i));
                    preNum = curPhone;
                }

                contactUsAdapter = new ContactUsAdapter(contactList, this);
                rvContactList.setLayoutManager(new LinearLayoutManager(this));
                rvContactList.setAdapter(contactUsAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pbContactList.setVisibility(View.GONE);
    }

    private void initViews() {
        pbContactList = findViewById(R.id.pbContactList);
        rvContactList = findViewById(R.id.rvContactList);
        contactList = new ArrayList<>();
        contactListTemp = new ArrayList<>();
        searchArray = new ArrayList<>();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search_contact, menu);

        searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchItem.setVisible(false);
        }
        if (searchView != null) {
            searchView.setSearchableInfo(Objects.requireNonNull(searchManager).getSearchableInfo(this.getComponentName()));
        }
        final SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                isSearchEnabled = true;
                searchArray.clear();
                if (contactList.size() > 0) {
                    for (int i = 0; i < contactList.size(); i++) {

                        if (contactList.get(i).getName() != null)
                            if (contactList.get(i).getName().length() > 0)
                                if (contactList.get(i).getName().toLowerCase().contains(newText.toLowerCase())) {
                                    Log.e("searchPos", contactList.get(i).getName());
                                    ContactListModel playListModel = contactList.get(i);
                                    searchArray.add(playListModel);
                                }
                    }

                    contactUsAdapter = new ContactUsAdapter(searchArray, ContactListActivity.this);
                    rvContactList.setLayoutManager(new LinearLayoutManager(ContactListActivity.this));
                    rvContactList.setAdapter(contactUsAdapter);
                }
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchArray.clear();
                return true;
            }
        };
        searchView.clearFocus();
        searchItem.collapseActionView();
        searchItem.setVisible(true);
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
