package com.barafu.common.activities.contactUs;

import android.os.Bundle;

import com.barafu.common.R;
import com.barafu.common.components.BaseActivity;

public class ContactUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
    }
}
