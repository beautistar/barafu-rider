package com.barafu.common.activities.emergency;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barafu.common.R;
import com.barafu.common.activities.emergency.contact_list.ContactListActivity;
import com.barafu.common.activities.emergency.contact_list.ContactListModel;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.database.SavedContactDatabase;
import com.barafu.common.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;

public class EmergencyActivity extends BaseActivity implements View.OnClickListener {

    Button btnAddContact;
    List<ContactListModel> contactListArray;
    LinearLayout llDescriptionLayout, llSavedContestList;

    RecyclerView rvEmergencyContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        initializeToolbar(getResources().getString(R.string.emergency_contact));

        initViews();
        btnAddContact.setOnClickListener(this);
        getSavedContacts();
    }

    private void getSavedContacts() {
        SavedContactDatabase savedContactDatabase = new SavedContactDatabase(this);
        Log.e("sizeOfSavedData", savedContactDatabase.getContacts().size() + "");

        if (contactListArray != null)
            contactListArray.clear();

        contactListArray = savedContactDatabase.getContacts();
        if (contactListArray.size() > 0) {
            llDescriptionLayout.setVisibility(View.GONE);
            llSavedContestList.setVisibility(View.VISIBLE);
            for (int i = 0; i < contactListArray.size(); i++) {
                Log.e("savedContactDatabase", contactListArray.get(i).getName() + "_______" + contactListArray.get(i).getPhoneNum());
            }

            SavedContactAdapter savedContactAdapter = new SavedContactAdapter(contactListArray, this);
            rvEmergencyContacts.setLayoutManager(new LinearLayoutManager(this));
            rvEmergencyContacts.setAdapter(savedContactAdapter);

        } else {
            llDescriptionLayout.setVisibility(View.VISIBLE);
            llSavedContestList.setVisibility(View.GONE);

        }

    }

    private void initViews() {
        contactListArray = new ArrayList<>();
        btnAddContact = findViewById(R.id.btnAddContact);
        llDescriptionLayout = findViewById(R.id.llDescriptionLayout);
        llSavedContestList = findViewById(R.id.llSavedContestList);
        rvEmergencyContacts = findViewById(R.id.rvEmergencyContacts);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CommonUtils.CONTACT_INTENT) {
            if (resultCode == Activity.RESULT_OK) {
                String phoneNumber = data.getStringExtra(CommonUtils.PHONE_NUMBER);
                String userName = data.getStringExtra(CommonUtils.USER_NAME);

                ContactListModel contactListModel = new ContactListModel(userName, phoneNumber);
                SavedContactDatabase savedContactDatabase = new SavedContactDatabase(this);
                savedContactDatabase.addOfflineTrackInfo(contactListModel);

                getSavedContacts();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAddContact) {
            if (checkingPermissionIsEnabledOrNot())
                startActivityForResult(new Intent(this, ContactListActivity.class), CommonUtils.CONTACT_INTENT);
            else
                requestMultiplePermission();
        }
    }

    private boolean checkingPermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(EmergencyActivity.this, READ_CONTACTS);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestMultiplePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]
                    {
                            READ_CONTACTS
                    }, CommonUtils.READ_CONTACT_PERMISION);
        }
    }

    private boolean checkCellPhonePermissionIsEnabledOrNot() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(EmergencyActivity.this, CALL_PHONE);
        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestCellPhoneMultiplePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]
                    {
                            CALL_PHONE
                    }, CommonUtils.READ_CONTACT_PERMISION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case CommonUtils.READ_CONTACT_PERMISION:
//                boolean raed = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                if (raed) {
//                    startActivityForResult(new Intent(this, ContactListActivity.class), CommonUtils.CONTACT_INTENT);
//                } else {
//
//                }
                break;
        }
    }

    public void onCallClicked(ContactListModel contactListModel) {
        if (checkCellPhonePermissionIsEnabledOrNot()) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + contactListModel.getPhoneNum()));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            startActivity(intent);
        } else {
            requestCellPhoneMultiplePermission();
        }

    }
}
