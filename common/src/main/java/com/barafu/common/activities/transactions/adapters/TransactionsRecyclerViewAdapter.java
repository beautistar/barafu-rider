package com.barafu.common.activities.transactions.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.barafu.common.MyTaxiApplication;
import com.barafu.common.databinding.ItemTransactionBinding;
import com.barafu.common.models.Transaction;
import com.barafu.common.utils.CommonUtils;

import java.util.List;

public class TransactionsRecyclerViewAdapter extends RecyclerView.Adapter<TransactionsRecyclerViewAdapter.ViewHolder> {
    private List<Transaction> transactions;

    public TransactionsRecyclerViewAdapter(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public TransactionsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemTransactionBinding itemBinding = ItemTransactionBinding.inflate(layoutInflater, parent, false);
        return new TransactionsRecyclerViewAdapter.ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(TransactionsRecyclerViewAdapter.ViewHolder holder, int position) {
        Transaction transaction = transactions.get(position);
        holder.bind(transaction);
//        android:text="@{@string/transaction_number(item.documentNumber)}"
        String cost = MyTaxiApplication.getCurrency()+" "+ CommonUtils.removeDecimal(transactions.get(position).getAmount());
        holder.binding.textTime.setText(cost);
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemTransactionBinding binding;
        ViewHolder(ItemTransactionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
        void bind(Transaction transaction) {
            binding.setItem(transaction);
            binding.executePendingBindings();
        }
    }
}