package com.barafu.common.activities.emergency.contact_list;

public class ContactListModel {
    String myName = "";
    String myNumber = "";

    public String getName() {
        return myName;
    }

    public void setName(String name) {
        myName = name;
    }

    public String getPhoneNum() {
        return myNumber;
    }

    public void setPhoneNum(String number) {
        myNumber = number;
    }
    public ContactListModel(String myName, String myNumber)
    {
        this.myName = myName;
        this.myNumber = myNumber;
    }
}
