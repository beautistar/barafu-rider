package com.barafu.common.activities.emergency.contact_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.barafu.common.R;
import com.barafu.common.activities.emergency.contact_list.ContactListModel;

import java.util.List;



public class ContactUsAdapter extends RecyclerView.Adapter<ContactUsAdapter.MyViewHolder> {
    private Context context;
    private List<ContactListModel> contactListArray;

    public ContactUsAdapter(List<ContactListModel> storedFileArray, Context context) {
        this.contactListArray = storedFileArray;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_contact_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        holder.tvContactName.setText(contactListArray.get(position).getName());
        holder.tvContactNumber.setText(contactListArray.get(position).getPhoneNum());

    }


    @Override
    public int getItemCount() {
        return contactListArray.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvContactName;

        TextView tvContactNumber;

        public MyViewHolder(View view) {
            super(view);

            tvContactName = view.findViewById(R.id.tvContactName);
            tvContactNumber = view.findViewById(R.id.tvContactNumber);

        }
    }


}
