package com.barafu.common.activities.support;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.barafu.common.R;
import com.barafu.common.components.BaseActivity;

public class SupportActivity extends BaseActivity {
    LinearLayout llAboutUs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        initializeToolbar("Support");
        initViews();

        llAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SupportActivity.this,AboutUsActivity.class));
            }
        });
    }

    private void initViews() {
        llAboutUs = findViewById(R.id.ll_about_us);
    }

}
