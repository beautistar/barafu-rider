package com.barafu.common.activities.support;

import android.os.Bundle;

import com.barafu.common.R;
import com.barafu.common.components.BaseActivity;

public class AboutUsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        initializeToolbar("About us");
    }
}
