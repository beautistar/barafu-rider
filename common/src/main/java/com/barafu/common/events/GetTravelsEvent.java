package com.barafu.common.events;

import com.barafu.common.utils.ServerResponse;

public class GetTravelsEvent extends BaseRequestEvent {
    public GetTravelsEvent() {
        super(new GetTravelsResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue(),null));
    }
}
