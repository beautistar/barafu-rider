package com.barafu.common.events;

public class EditProfileInfoResultEvent extends BaseResultEvent {
    public EditProfileInfoResultEvent(int response) {
        super(response);
    }
}
