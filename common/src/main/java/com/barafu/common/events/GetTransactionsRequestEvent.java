package com.barafu.common.events;

public class GetTransactionsRequestEvent extends BaseRequestEvent {
    public GetTransactionsRequestEvent() {
        super(new GetTransactionsResultEvent());
    }
}
