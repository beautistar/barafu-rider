package com.barafu.common.events;

public class ServiceCallRequestResultEvent extends BaseResultEvent {
    public ServiceCallRequestResultEvent(int code) {
        super(code);
    }
}
