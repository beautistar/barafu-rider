package com.barafu.common.events;

public class WriteComplaintResultEvent extends BaseResultEvent {
    public WriteComplaintResultEvent(int code) {
        super(code);
    }
}
