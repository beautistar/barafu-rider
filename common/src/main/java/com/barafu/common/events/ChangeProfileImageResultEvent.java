package com.barafu.common.events;

import com.google.gson.Gson;
import com.barafu.common.models.Media;

public class ChangeProfileImageResultEvent extends BaseResultEvent {
    public Media media;

    public ChangeProfileImageResultEvent(int code, String media) {
        super(code);
        this.media = new Gson().fromJson(media,Media.class);
    }
}
