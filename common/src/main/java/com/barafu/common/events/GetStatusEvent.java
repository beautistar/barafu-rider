package com.barafu.common.events;

import com.barafu.common.utils.ServerResponse;

public class GetStatusEvent extends BaseRequestEvent {
    public GetStatusEvent() {
        super(new GetStatusResultEvent(ServerResponse.REQUEST_TIMEOUT));
    }
}
