package com.barafu.common.events;

import com.barafu.common.utils.ServerResponse;

public class ServiceCallRequestEvent extends BaseRequestEvent {
    public ServiceCallRequestEvent() {
        super(new ServiceCallRequestResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
    }
}
