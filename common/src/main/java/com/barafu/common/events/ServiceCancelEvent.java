package com.barafu.common.events;

import com.barafu.common.utils.ServerResponse;

public class ServiceCancelEvent extends BaseRequestEvent {
    public ServiceCancelEvent(){
        super(new ServiceCallRequestResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));

    }
}
