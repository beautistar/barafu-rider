package com.barafu.common.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.barafu.common.activities.emergency.contact_list.ContactListModel;

import java.util.ArrayList;

public class SavedContactDatabase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String SAVED_CONTACT = "saved_contact";

    private Context context;

    // practice_tracks Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_PHONE= "user_phone";
//    private static final String CURSOR_END = "cursor_end";
//    private static final String AUDIO_ID = "audio_id";
//    private static final String IS_TRACK_COMPLETE = "is_track_complete";
//    private static final String CURRENT_TRACK_DATE = "current_track_date";
//    private static final String DEVICE_USED = "device_used";
//    private static final String EVENT = "event";
//    private static final String CLASS_ID = "class_id";
//    private static final String IS_ADDITIONAL_PRACTICE = "program_language";
    // Database Name
    private static final String DATABASE_NAME = "saved_contact_database";

    public SavedContactDatabase(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PRACTICE_TABLE = "CREATE TABLE " + SAVED_CONTACT + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_USER_NAME + " TEXT,"
//                + CURSOR_END + " TEXT,"
//                + AUDIO_ID + " TEXT,"
//                + KEY_PROGRAM_ID + " TEXT,"
//                + IS_TRACK_COMPLETE + " TEXT,"
//                + CURRENT_TRACK_DATE + " TEXT,"
//                + DEVICE_USED + " TEXT,"
//                + EVENT + " TEXT,"
//                + CLASS_ID + " TEXT,"
                + KEY_USER_PHONE + " TEXT" + ")";
        db.execSQL(CREATE_PRACTICE_TABLE);

    }

    public void addOfflineTrackInfo(ContactListModel contactListModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_USER_NAME, contactListModel.getName());
        values.put(KEY_USER_PHONE, contactListModel.getPhoneNum());
//        values.put(AUDIO_ID, offlineTrackInfoModel.getAudioId());
//        values.put(KEY_PROGRAM_ID, offlineTrackInfoModel.getProgramId());
//        values.put(IS_TRACK_COMPLETE, offlineTrackInfoModel.getIsComplete());
//        values.put(CURRENT_TRACK_DATE, offlineTrackInfoModel.getClientDate());
//        values.put(DEVICE_USED, offlineTrackInfoModel.getDeviceUsed());
//        values.put(EVENT, offlineTrackInfoModel.getEvent());
//        values.put(CLASS_ID, offlineTrackInfoModel.getClassId());
//
//
        db.insert(SAVED_CONTACT, null, values);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + SAVED_CONTACT);

        onCreate(db);
    }

    public ArrayList<ContactListModel> getContacts() {
        ArrayList<ContactListModel> practiceTracks = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + SAVED_CONTACT;
//                + " WHERE " + KEY_USER_PHONE + " = " +;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ContactListModel offlineTrackInfoModel = new ContactListModel(
                        cursor.getString(1),
                        cursor.getString(2)
//                        cursor.getString(3),
//                        cursor.getString(4),
//                        cursor.getString(5),
//                        cursor.getString(6),
//                        cursor.getString(7),
//                        cursor.getString(8),
//                        cursor.getString(9)
                );



                practiceTracks.add(offlineTrackInfoModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return practiceTracks;
    }


//    public void deleteAllTracks() {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        db.delete(SAVED_CONTACT, KEY_USER_ID +" = "+ new SessionManager(context).getUserId(),
//                new String[]{});
//
//        db.close();
//    }

}

