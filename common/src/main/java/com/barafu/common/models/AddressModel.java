package com.barafu.common.models;

public class AddressModel {
    String address;
    String addressID;
    String placeID;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressID() {
        return addressID;
    }

    public void setAddressID(String addressID) {
        this.addressID = addressID;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public AddressModel(String addressID, String address, String placeID)
    {
        this.placeID = placeID;
        this.addressID = addressID;
        this.address = address;
    }
}
