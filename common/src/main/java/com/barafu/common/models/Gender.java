package com.barafu.common.models;


public enum Gender {
    unknown,
    male,
    female
}
