package com.barafu.common.interfaces;

public interface LocationChangeListener {
    void OnLocationChange(String lat, String lng);
}
