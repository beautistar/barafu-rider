package com.barafu.driver.events;

import com.barafu.common.models.Travel;

public class SendTravelInfoEvent {
    public Travel travel;
    public SendTravelInfoEvent(Travel travel) {
        this.travel = travel;
    }
}
