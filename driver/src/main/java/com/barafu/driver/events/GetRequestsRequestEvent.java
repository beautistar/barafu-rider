package com.barafu.driver.events;

import com.barafu.common.events.BaseRequestEvent;

public class GetRequestsRequestEvent extends BaseRequestEvent {
    public GetRequestsRequestEvent() {
        super(new GetRequestsResultEvent());
    }
}
