package com.barafu.driver.events;

import com.barafu.common.models.Travel;

public class RiderAcceptedEvent {
    public Travel travel;

    public RiderAcceptedEvent(Object... args) {
        this.travel = Travel.fromJson(args[0].toString());
    }
}
