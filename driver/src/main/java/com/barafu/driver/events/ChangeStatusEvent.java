package com.barafu.driver.events;

import com.barafu.common.events.BaseRequestEvent;
import com.barafu.common.utils.ServerResponse;

public class ChangeStatusEvent extends BaseRequestEvent {
    public ChangeStatusEvent(Status status) {
        super(new ChangeStatusResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue()));
        this.status = status;
    }
    public enum Status {
        ONLINE("online"),
        OFFLINE("offline");
        private String value;

        Status(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    public Status status;
}
