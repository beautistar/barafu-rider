package com.barafu.driver.events;

import com.barafu.common.events.BaseResultEvent;
import com.barafu.common.models.Travel;

public class AcceptOrderResultEvent extends BaseResultEvent {
    public Travel travel;
    public AcceptOrderResultEvent(Object... args) {
        super(args);
        travel = Travel.fromJson(args[0].toString());
    }
}
