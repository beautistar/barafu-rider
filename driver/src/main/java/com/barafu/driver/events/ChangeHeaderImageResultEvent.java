package com.barafu.driver.events;

import com.google.gson.Gson;
import com.barafu.common.events.BaseResultEvent;
import com.barafu.common.models.Media;

public class ChangeHeaderImageResultEvent extends BaseResultEvent {
    public Media media;
    public ChangeHeaderImageResultEvent(int code, String media) {
        super(code);
        this.media = new Gson().fromJson(media,Media.class);
    }
}
