package com.barafu.driver.events;

import com.barafu.common.events.BaseRequestEvent;
import com.barafu.common.utils.ServerResponse;

public class ChangeHeaderImageEvent extends BaseRequestEvent {
    public String path;
    public ChangeHeaderImageEvent(String path){
        super(new ChangeHeaderImageResultEvent(ServerResponse.REQUEST_TIMEOUT.getValue(),null));
        this.path = path;
    }
}
