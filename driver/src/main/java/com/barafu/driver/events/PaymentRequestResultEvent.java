package com.barafu.driver.events;

import com.barafu.common.events.BaseResultEvent;

public class PaymentRequestResultEvent extends BaseResultEvent {
    public PaymentRequestResultEvent(int code) {
        super(code);
    }
}
