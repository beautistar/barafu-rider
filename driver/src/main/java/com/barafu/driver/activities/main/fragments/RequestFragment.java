package com.barafu.driver.activities.main.fragments;


import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.barafu.common.MyTaxiApplication;
import com.barafu.common.models.Travel;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.LatLngDeserializer;
import com.barafu.common.utils.LocationHelper;
import com.barafu.driver.R;
import com.barafu.driver.databinding.FragmentRequestBinding;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.type.LatLng;

import java.lang.reflect.Type;

public class RequestFragment extends Fragment {
    private Travel travel;
    FragmentRequestBinding binding;
    private CountDownTimer countDownTimer;
    private RequestFragment.OnFragmentInteractionListener mListener;
    private static final String ARG_REQUEST = "request";
    private TextView tvRiderName;

    public RequestFragment() {

    }

    public static RequestFragment newInstance(Travel travel) {
        RequestFragment fragment = new RequestFragment();
        Bundle args = new Bundle();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LatLng.class, new LatLngDeserializer());
        Gson customGson = gsonBuilder.create();
        args.putString(ARG_REQUEST, customGson.toJson(travel));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String travelString = getArguments().getString(ARG_REQUEST);
            Log.e("travelString111",travelString);
            Type type = new TypeToken<Travel>() {
            }.getType();
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(LatLng.class, new LatLngDeserializer());
            Gson customGson = gsonBuilder.create();
            travel = customGson.fromJson(travelString, type);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_request, container, false);
        binding.setTravel(travel);
        tvRiderName = binding.getRoot().findViewById(R.id.tvRiderName);
        if (CommonUtils.currentLocation != null)
            locationChanged(CommonUtils.currentLocation);
        countDownTimer = new CountDownTimer(5 * 60000, 50) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                if (mListener != null && travel != null) {
                    mListener.onDecline(travel);
                    mListener.onInvisible(travel);
                }
            }
        };
        countDownTimer.start();
        if (getResources().getBoolean(R.bool.use_miles)) {
            binding.textUserDestinationDistance.setText(getString(R.string.unit_distance_miles, travel.getDistanceBest() / 1609.344f));
        } else {
            binding.textUserDestinationDistance.setText(getString(R.string.unit_distance, travel.getDistanceBest() / 1000f));
        }
        String cost = CommonUtils.addSaperator(CommonUtils.removeDecimal(travel.getCostBest()));
        String calCost = MyTaxiApplication.getCurrency() + " " + cost;

        Log.e("travel.getCostBest()", travel.getCostBest() + "");
        binding.textCost.setText(calCost);
//        android:text='@{@string/unit_money_one(travel.costBest)}'
//        String riderName = travel.getRider().getFirstName() + " ";
//        Log.e("riderName", riderName+ "");
//        tvRiderName.setText(riderName);
        binding.buttonAccept.setOnClickListener(view -> {
            countDownTimer.cancel();
            mListener.onAccept(travel);
        });
        binding.buttonDecline.setOnClickListener(view -> {
            countDownTimer.cancel();
            if (mListener != null && travel != null) {
                mListener.onDecline(travel);
//                mListener.onInvisible(travel);
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        // mListener.onVisible(this.travel);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onInvisible(this.travel);
        mListener = null;
    }

    public void locationChanged(com.google.android.gms.maps.model.LatLng latLng) {
        int distanceDriver = LocationHelper.distFrom(travel.getPickupPoint(), latLng);
        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) binding.getRoot().findViewById(R.id.guideline_start).getLayoutParams();
        lp.guidePercent = ((float) distanceDriver / (travel.getDistanceBest() + distanceDriver));
        binding.getRoot().findViewById(R.id.guideline_start).setLayoutParams(lp);
        if (getResources().getBoolean(R.bool.use_miles)) {
            binding.textDriverUserDistance.setText(getString(R.string.unit_distance_miles, distanceDriver / 1609.344f));
        } else {
            binding.textDriverUserDistance.setText(getString(R.string.unit_distance, distanceDriver / 1000f));

        }
    }

    public interface OnFragmentInteractionListener {
        void onAccept(Travel travel);

        void onDecline(Travel travel);

        void onVisible(Travel travel);

        void onInvisible(Travel travel);
    }
}
