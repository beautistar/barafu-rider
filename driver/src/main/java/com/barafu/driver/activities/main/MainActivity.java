package com.barafu.driver.activities.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.barafu.common.MyTaxiApplication;
import com.barafu.common.activities.chargeAccount.ChargeAccountActivity;
import com.barafu.common.activities.emergency.EmergencyActivity;
import com.barafu.common.activities.support.SupportActivity;
import com.barafu.common.activities.transactions.TransactionsActivity;
import com.barafu.common.activities.travels.TravelsActivity;
import com.barafu.common.components.LoadingDialog;
import com.barafu.common.events.AcceptOrderEvent;
import com.barafu.common.events.GetStatusEvent;
import com.barafu.common.events.GetStatusResultEvent;
import com.barafu.common.events.NotificationPlayerId;
import com.barafu.common.events.ProfileInfoChangedEvent;
import com.barafu.common.models.Travel;
import com.barafu.common.utils.AlertDialogBuilder;
import com.barafu.common.utils.AlerterHelper;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.DataBinder;
import com.barafu.common.utils.MyPreferenceManager;
import com.barafu.common.utils.ServerResponse;
import com.barafu.driver.R;
import com.barafu.driver.activities.about.AboutActivity;
import com.barafu.driver.activities.main.adapters.RequestsFragmentPagerAdapter;
import com.barafu.driver.activities.main.fragments.RequestFragment;
import com.barafu.driver.activities.profile.ProfileActivity;
import com.barafu.driver.activities.statistics.StatisticsActivity;
import com.barafu.driver.activities.travel.TravelActivity;
import com.barafu.driver.activities.worker.MyWorker;
import com.barafu.driver.databinding.ActivityMainBinding;
import com.barafu.driver.events.AcceptOrderResultEvent;
import com.barafu.driver.events.CancelRequestEvent;
import com.barafu.driver.events.ChangeStatusEvent;
import com.barafu.driver.events.ChangeStatusResultEvent;
import com.barafu.driver.events.GetRequestsRequestEvent;
import com.barafu.driver.events.GetRequestsResultEvent;
import com.barafu.driver.events.LocationChangedEvent;
import com.barafu.driver.events.RequestReceivedEvent;
import com.barafu.driver.events.RiderAcceptedEvent;
import com.barafu.driver.events.StartTrackingRequestEvent;
import com.barafu.driver.events.StopTrackingRequestEvent;
import com.barafu.driver.ui.DriverBaseActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class MainActivity extends DriverBaseActivity implements OnMapReadyCallback, LocationListener, RequestFragment.OnFragmentInteractionListener, OSSubscriptionObserver {
    MyPreferenceManager SP;
    private GoogleMap mMap;
    Marker markerDriver;
    Marker markerPickup;
    Marker markerDropOff;
    public ActivityMainBinding binding;
    private RequestsFragmentPagerAdapter requestCardsAdapter;
    static final int ACTIVITY_PROFILE = 11;
    static final int ACTIVITY_WALLET = 12;
    static final int ACTIVITY_TRAVEL = 14;
    SupportMapFragment mapFragment;
    private MaterialDialog loadingRequestsLoadingDialog;
    private int createdCount = 0;
    ExampleBroadcastReceiver exampleBroadcastReceiver = new ExampleBroadcastReceiver();
    private Handler mHandler;

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createdCount = 1;
        OneSignal.addSubscriptionObserver(this);
        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("customreceiveraction");
        if (exampleBroadcastReceiver != null)
            registerReceiver(exampleBroadcastReceiver, intentFilter);


        initViews();
        requestCardsAdapter = new RequestsFragmentPagerAdapter(getSupportFragmentManager(), new ArrayList<>());
        binding.requestsViewPager.setAdapter(requestCardsAdapter);
        binding.requestsViewPager.setOffscreenPageLimit(3);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SP = MyPreferenceManager.getInstance(this.getApplicationContext());
        setSupportActionBar(binding.appbar);
        ActionBar actionBar = getSupportActionBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(getApplicationContext(), R.color.accent_black));
        }
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        binding.navigationView.setNavigationItemSelectedListener(menuItem -> {
            binding.drawerLayout.closeDrawers();
            switch (menuItem.getItemId()) {
                case (R.id.nav_item_travels):
                    startActivity(new Intent(MainActivity.this, TravelsActivity.class));
                    break;
                case (R.id.nav_item_profile):
                    startActivityForResult(new Intent(MainActivity.this, ProfileActivity.class), ACTIVITY_PROFILE);
                    break;
                case (R.id.nav_item_statistics):
                    startActivity(new Intent(MainActivity.this, StatisticsActivity.class));
                    break;
                case (R.id.nav_item_charge_account):
                    startActivityForResult(new Intent(MainActivity.this, ChargeAccountActivity.class), ACTIVITY_WALLET);
                    break;
                case (R.id.nav_item_transactions):
                    startActivity(new Intent(MainActivity.this, TransactionsActivity.class));
                    break;
                case (R.id.nav_item_about):
                    startActivity(new Intent(MainActivity.this, AboutActivity.class));
                    break;
                case (R.id.nav_item_support):
                    startActivity(new Intent(MainActivity.this, SupportActivity.class));
                    break;
                case (R.id.nav_item_emergency):
                    startActivity(new Intent(MainActivity.this, EmergencyActivity.class));
                    break;
                case (R.id.nav_item_exit):
                    logout();
                    break;
                default:
                    Toast.makeText(MainActivity.this, menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        });
        fillInfo();
        eventBus.post(new GetStatusEvent());

        if (getDriverStatus().equals(CommonUtils.ONLINE))
            binding.switchConnection.setChecked(true);
        else
            binding.switchConnection.setChecked(false);
        binding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);

        setHandlerForStatus();

//        setStatusWorker();
//        setDriverOnlinOffline();
    }

    private void setHandlerForStatus() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mHandler.postDelayed(this, 60000);
                if (createdCount != 1) {

                    if (getDriverStatus().equals(CommonUtils.ONLINE))
                    {
                        Log.e("getDriverStatus()",getDriverStatus());

                        eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
                        eventBus.post(new StartTrackingRequestEvent());
                        eventBus.post(new LocationChangedEvent(markerDriver.getPosition()));
                    }
//                    else
//                        binding.switchConnection.setChecked(false);
                }

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (loadingRequestsLoadingDialog !=null) {
//            if (loadingRequestsLoadingDialog.isShowing()) {
//                loadingRequestsLoadingDialog.dismiss();
//            }
//        }


//        loadingRequestsLoadingDialog = new MaterialDialog.Builder(this)
//                .title("Reloading status")
//                .content("Please wait...")
//                .progress(true, 0)
//                .cancelable(true)
//                .show();


//        Log.e("createCount", String.valueOf(createdCount));
//        if (createdCount != 1) {
//            restartActivity();
//        }


        Log.e("getDriverStatus()", getDriverStatus());
        if (createdCount != 1) {
            if(getDriverStatus().equals(CommonUtils.ONLINE))
                binding.switchConnection.setChecked(true);
            else
                binding.switchConnection.setChecked(false);

        }
        createdCount = 0;
        eventBus.post(new GetRequestsRequestEvent());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (exampleBroadcastReceiver != null)
            unregisterReceiver(exampleBroadcastReceiver);

        mHandler.removeCallbacksAndMessages(null);


    }

    //    private void setDriverOnlinOffline() {
//        if (getDriverStatus().equalsIgnoreCase(CommonUtils.ONLINE)) {
//            eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
//            eventBus.post(new StartTrackingRequestEvent());
////            eventBus.post(new LocationChangedEvent(markerDriver.getPosition()));
//        } else {
//            eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.OFFLINE));
//            eventBus.post(new StopTrackingRequestEvent());
//        }
//
//    }

    private void initViews() {
        isMarkerRotating = false;
        mHandler = new Handler();

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setTrafficEnabled(true);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        getLastKnownLocation();
        if (getResources().getBoolean(R.bool.isNightMode)) {
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_night));
            if (!success)
                Log.e("MapsActivityRaw", "Style parsing failed.");
        }
    }

    public void moveDriverPin(double lat, double lng) {
        LatLng driver = new LatLng(lat, lng);
        float bearing = (float) bearingBetweenLocations(markerDriver.getPosition(), driver);
        rotateMarker(markerDriver, bearing);

        markerDriver.setPosition(driver);
        CommonUtils.currentLocation = driver;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                binding.drawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestsReceived(GetRequestsResultEvent event) {

        Log.e("RequestReceived===", "Received");
        Log.e("event.response", event.response + "");
        //loadingRequestsLoadingDialog.dismiss();
        if (event.response == ServerResponse.DRIVER_IS_OFFLINE) {
//            binding.switchConnection.setOnCheckedChangeListener(null);
//            binding.switchConnection.setChecked(false);
//            binding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
            return;
        }
//        binding.switchConnection.setOnCheckedChangeListener(null);
        binding.switchConnection.setChecked(true);
//        binding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
        requestCardsAdapter = new RequestsFragmentPagerAdapter(getSupportFragmentManager(), event.travels);
        binding.requestsViewPager.setAdapter(requestCardsAdapter);
        binding.requestsViewPager.setOffscreenPageLimit(3);


        /*if(event.requests.size() > 0) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAnotherDriverAcceptedRequest(CancelRequestEvent event) {
        LoadingDialog.dismiss();
        int position = requestCardsAdapter.getPositionWithTravelId(event.travelId);
        if (position >= 0)
            requestCardsAdapter.remove(position);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRequestReceived(RequestReceivedEvent event) {
        Log.d("Reqest received event", event.toString());
        requestCardsAdapter.add(event.travel);
        requestCardsAdapter.notifyDataSetChanged();
        /*BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onProfileChanged(ProfileInfoChangedEvent event) {
        fillInfo();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatusChanged(ChangeStatusResultEvent event) {
        Log.e("onStatusChanged", "onStatusChanged");
        if (event.hasError()) {
            event.showError(MainActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
//                    onConnectionSwitchChanged.onCheckedChanged(null, binding.switchConnection.isChecked());
                } else {
                    binding.switchConnection.setEnabled(true);
//                    binding.switchConnection.setOnCheckedChangeListener(null);
//                    binding.switchConnection.setChecked(!binding.switchConnection.isChecked());
//                    binding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
                }
            });
            return;
        }
        binding.switchConnection.setEnabled(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRiderAccepted(RiderAcceptedEvent event) {
        Log.d("onRiderAccepted", event.toString());
        LoadingDialog.dismiss();
        Intent intentTravel = new Intent(MainActivity.this, TravelActivity.class);
        Log.e("onRiderAccepted", event.travel.toJson() + "");
        intentTravel.putExtra("travel", event.travel.toJson());
        intentTravel.putExtra("driverLat", markerDriver.getPosition().latitude);
        intentTravel.putExtra("driverLng", markerDriver.getPosition().longitude);
        startActivityForResult(intentTravel, ACTIVITY_TRAVEL);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDriverAcceptedResult(AcceptOrderResultEvent event) {
        Log.d("onDriverAcceptedResult", event.travel.toString());
        Intent intentTravel = new Intent(MainActivity.this, TravelActivity.class);
        intentTravel.putExtra("travel", event.travel.toJson());
        intentTravel.putExtra("driverLat", markerDriver.getPosition().latitude);
        intentTravel.putExtra("driverLng", markerDriver.getPosition().longitude);
        startActivityForResult(intentTravel, ACTIVITY_TRAVEL);
    }

    private CompoundButton.OnCheckedChangeListener onConnectionSwitchChanged = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            Log.e("ischeckedCalled", binding.switchConnection.isChecked() + "");
            if (binding.switchConnection.isChecked()) {
                setDriverStatus(CommonUtils.ONLINE);

                eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.ONLINE));
                eventBus.post(new StartTrackingRequestEvent());
                eventBus.post(new LocationChangedEvent(markerDriver.getPosition()));
            } else {
                setDriverStatus(CommonUtils.OFFLINE);
                eventBus.post(new ChangeStatusEvent(ChangeStatusEvent.Status.OFFLINE));
                eventBus.post(new StopTrackingRequestEvent());
                binding.switchConnection.setEnabled(false);

            }
        }
    };

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager manager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers;
        if (manager != null) {
            providers = manager.getProviders(true);
        } else
            return;
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = manager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        LatLng latLng;
        if (bestLocation == null)
            latLng = new LatLng(Float.parseFloat(getString(R.string.defaultLocation).split(",")[0]), Float.parseFloat(getString(R.string.defaultLocation).split(",")[1]));
        else
            latLng = new LatLng(bestLocation.getLatitude(), bestLocation.getLongitude());
        if (markerDriver == null) {

            markerDriver = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.pin_driver))));
            float bearing = (float) bearingBetweenLocations(markerDriver.getPosition(), latLng);
            rotateMarker(markerDriver, bearing);
        } else
            markerDriver.setPosition(latLng);
        if (binding.switchConnection.isChecked())
            eventBus.post(new LocationChangedEvent(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));

    }

    private void fillInfo() {
        try {
            String name;
            if (CommonUtils.driver.getStatus() != null && CommonUtils.driver.getStatus().equals("blocked")) {
                logout();
                return;
            }
            if ((CommonUtils.driver.getFirstName() == null || CommonUtils.driver.getFirstName().isEmpty()) && (CommonUtils.driver.getLastName() == null || CommonUtils.driver.getLastName().isEmpty()))
                name = String.valueOf(CommonUtils.driver.getMobileNumber());
            else
                name = CommonUtils.driver.getFirstName() + " " + CommonUtils.driver.getLastName();
            View header = binding.navigationView.getHeaderView(0);
            ((TextView) header.findViewById(R.id.navigation_header_name)).setText(name);
//            String balance = getString(R.string.drawer_header_balance, CommonUtils.driver.getBalance())+" "+ MyTaxiApplication.getCurrency();
            String cost = "Credit: " + MyTaxiApplication.getCurrency() + " " + CommonUtils.addSaperator(CommonUtils.removeDecimal(CommonUtils.driver.getBalance()) + "");
            Log.e("getCurrency()", MyTaxiApplication.getCurrency() + "_aaa");
            ((TextView) header.findViewById(R.id.navigation_header_charge)).setText(cost);
//            ((TextView) header.findViewById(R.id.navigation_header_charge)).setText(getString(R.string.drawer_header_balance, CommonUtils.driver.getBalance()));
            ImageView imageView = header.findViewById(R.id.navigation_header_image);
            ImageView headerView = header.findViewById(R.id.navigation_background);
            DataBinder.setMedia(imageView, CommonUtils.driver.getMedia());
            DataBinder.setMedia(headerView, CommonUtils.driver.getCarMedia());
        } catch (Exception ignored) {
        }
    }

    private void logout() {
        SP.putString("driver_token", "");
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (ACTIVITY_PROFILE):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.info_edit_profile_success));
                fillInfo();
                break;

            case (ACTIVITY_WALLET):
                if (resultCode == RESULT_OK)
                    AlerterHelper.showInfo(MainActivity.this, getString(R.string.account_charge_success));
                fillInfo();
                break;

            case (ACTIVITY_TRAVEL):
                /*binding.switchConnection.setOnCheckedChangeListener(null);
                binding.switchConnection.setChecked(true);
                binding.switchConnection.setOnCheckedChangeListener(onConnectionSwitchChanged);
                onConnectionSwitchChanged.onCheckedChanged(binding.switchConnection, binding.switchConnection.isChecked());*/
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (binding.switchConnection.isChecked())
            eventBus.post(new LocationChangedEvent(latLng));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, mMap.getCameraPosition().zoom > 5 ? mMap.getCameraPosition().zoom : 18);
        mMap.animateCamera(cameraUpdate);
        moveDriverPin(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onAccept(Travel travel) {

        eventBus.post(new AcceptOrderEvent(travel.getId(), travel.getCostBest()));
        removeMarkers();
        while (requestCardsAdapter.getCount() > 0)
            requestCardsAdapter.remove(0);
    }

    @Subscribe(threadMode = MAIN)
    public void OnGetStatusResultReceived(GetStatusResultEvent event) {
        if (event.hasError())
            return;
        AlertDialogBuilder.show(MainActivity.this, getString(R.string.recovery_travel_driver), getString(R.string.message_default_title), AlertDialogBuilder.DialogButton.OK, result -> {
            Intent intent = new Intent(MainActivity.this, TravelActivity.class);
            intent.putExtra("travel", event.travel.toJson());
            intent.putExtra("driverLat", markerDriver.getPosition().latitude);
            intent.putExtra("driverLng", markerDriver.getPosition().longitude);
            startActivityForResult(intent, ACTIVITY_TRAVEL);
        });
    }

    @Override
    public void onDecline(Travel travel) {
        int position = requestCardsAdapter.getPositionWithTravelId(travel.getId());
        if (position >= 0)
            requestCardsAdapter.remove(position);
        requestCardsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onVisible(Travel travel) {
        if (markerPickup == null) {
            markerPickup = mMap.addMarker(new MarkerOptions()
                    .position(travel.getPickupPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_pickup)));
        } else {
            markerPickup.setPosition(travel.getPickupPoint());
        }
        if (markerDropOff == null) {
            markerDropOff = mMap.addMarker(new MarkerOptions()
                    .position(travel.getDestinationPoint())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_destination)));
        } else {
            markerDropOff.setPosition(travel.getDestinationPoint());
        }
        List<LatLng> latLngs = new ArrayList<>();
        latLngs.add(travel.getPickupPoint());
        latLngs.add(travel.getDestinationPoint());
        latLngs.add(markerDriver.getPosition());
        mMap.setPadding(0, 0, 0, 850);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng location : latLngs)
            builder.include(location);
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
        mMap.animateCamera(cu);
        //((RequestFragment)requestCardsAdapter.getFragment(travel,requestCardsAdapter.getPositionWithTravelId(travel.getId()))).locationChanged(markerDriver.getPosition());
    }

    @Override
    public void onInvisible(Travel travel) {
        removeMarkers();
    }

    private void removeMarkers() {
        if (markerPickup != null)
            markerPickup.remove();
        if (markerDropOff != null)
            markerDropOff.remove();
        markerPickup = null;
        markerDropOff = null;
        mMap.setPadding(0, 0, 0, 0);
    }


    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom().getSubscribed() && stateChanges.getTo().getSubscribed())
            eventBus.post(new NotificationPlayerId(stateChanges.getTo().getUserId()));
    }

    public void onRefreshRequestsClicked(View view) {
        /*eventBus.post(new GetRequestsRequestEvent());
        loadingRequestsLoadingDialog = new MaterialDialog.Builder(this)
                .title("Reloading status")
                .content("Please wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();*/
    }

    public class ExampleBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("onReceiveCalled", "onReceiveCalled");

            if (intent != null) {
                if (intent.hasExtra("action")) {
                    String action = intent.getStringExtra("action");
                    Log.e("action",action);
                    if (action.equals("update")) {
                        if (createdCount != 1) {
                            if (getDriverStatus().equals(CommonUtils.ONLINE))
                                binding.switchConnection.setChecked(true);
                            else
                                binding.switchConnection.setChecked(false);
                        }

                    }
                }
            }
        }
    }

    private void setStatusWorker() {
        Log.e("workerCalled", "workerCalled");
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(MyWorker.class, 20, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance(this).enqueueUniquePeriodicWork("Online", ExistingPeriodicWorkPolicy.KEEP, saveRequest);

    }

}
