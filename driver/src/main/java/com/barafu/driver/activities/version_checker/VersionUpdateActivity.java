package com.barafu.driver.activities.version_checker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.barafu.common.BuildConfig;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.utils.ConnectivityReceiver;
import com.barafu.driver.R;
import com.barafu.driver.activities.splash.SplashActivity;

import org.jsoup.Jsoup;

import java.io.IOException;

public class VersionUpdateActivity extends BaseActivity {
    Dialog changeVersionDialog;

    Button btnSkipUpdate, btnUpdateVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_update);
        initializeToolbar(getString(R.string.app_name));
        initViews();
    }

    private void initViews() {
        btnSkipUpdate = findViewById(R.id.btnSkipUpdate);
        btnUpdateVersion = findViewById(R.id.btnUpdateVersion);

        btnSkipUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VersionUpdateActivity.this, SplashActivity.class));
                finish();

            }
        });
        btnUpdateVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        showSnack(ConnectivityReceiver.isConnected());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(VersionUpdateActivity.this, SplashActivity.class));
        finish();

    }

    private void showSnack(boolean isConnected) {
        if (!isConnected) {
            AlertDialog.Builder builder = new AlertDialog.Builder(VersionUpdateActivity.this);
            builder.setMessage("There seems to be an issue conecting to internet.\nPlease make sure you're connected to the internet.");
            builder.setPositiveButton("OK", (dialog, id) -> {
                dialog.dismiss();
                if (ConnectivityReceiver.isConnected()) {

                }
            });
            builder.setNegativeButton("NO", (dialog, id) -> {
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        } else
            checkVersion();
    }

    private void checkVersion() {
        String version_name = BuildConfig.VERSION_NAME;

//        VersionChecker versionChecker = new VersionChecker();
//        try {
//            String latestVersion = versionChecker.execute().get();
        String latestVersion = "17";
        Log.e("latestVersion", latestVersion + " ---- " + version_name);
        if (version_name.equals(latestVersion)) {
//                startHomeScreen();
        } else {
//                startHomeScreen();
            showUpdateAlartDialog();
        }

//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }

    }

    public class VersionChecker extends AsyncTask<String, String, String> {

        String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

    }

    private void showUpdateAlartDialog() {
        // custom changeVersionDialog
        changeVersionDialog = new Dialog(VersionUpdateActivity.this);
        changeVersionDialog.setContentView(R.layout.dialog_update);
        changeVersionDialog.setTitle("New Version Available");
        changeVersionDialog.setCancelable(false);
        // set the custom changeVersionDialog components - text, image and button
        TextView tvUpdateProject = changeVersionDialog.findViewById(R.id.tvUpdateProject);
        TextView tvCancelUpdate = changeVersionDialog.findViewById(R.id.tvCancelUpdate);

        tvUpdateProject.setOnClickListener(v -> {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }

        });
        tvCancelUpdate.setOnClickListener(view -> {
            changeVersionDialog.dismiss();
            finish();
        });
        changeVersionDialog.show();
    }

}
