package com.barafu.driver.activities.travel;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.transition.TransitionManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.barafu.common.MyTaxiApplication;
import com.barafu.common.components.LoadingDialog;
import com.barafu.common.events.ServiceCallRequestEvent;
import com.barafu.common.events.ServiceCallRequestResultEvent;
import com.barafu.common.events.ServiceCancelEvent;
import com.barafu.common.events.ServiceCancelResultEvent;
import com.barafu.common.location.MapHelper;
import com.barafu.common.models.Rider;
import com.barafu.common.models.Travel;
import com.barafu.common.utils.AlertDialogBuilder;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.DirectionsJSONParser;
import com.barafu.common.utils.LocationHelper;
import com.barafu.common.utils.MapDirectionsParser;
import com.barafu.driver.R;
import com.barafu.driver.databinding.ActivityTravelBinding;
import com.barafu.driver.events.LocationChangedEvent;
import com.barafu.driver.events.SendTravelInfoEvent;
import com.barafu.driver.events.ServiceFinishEvent;
import com.barafu.driver.events.ServiceFinishResultEvent;
import com.barafu.driver.events.ServiceInLocationEvent;
import com.barafu.driver.events.ServiceStartEvent;
import com.barafu.driver.ui.DriverBaseActivity;
import com.google.android.gms.common.internal.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;


import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


import java.net.URI;
import java.math.BigDecimal;

public class TravelActivity extends DriverBaseActivity implements OnMapReadyCallback {
    Travel travel;
    GoogleMap gMap;
    boolean endTravel = false;
    LatLng currentLocation;
    ActivityTravelBinding binding;
    Marker currentMarker;
    Marker destinationMarker;
    DirectionsJSONParser directionToPassengerRouter;
    List<LatLng> geoLog = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_travel);
        Log.e("fromJsonValues", getIntent().getStringExtra("travel"));
        travel = Travel.fromJson(getIntent().getStringExtra("travel"));
        Log.e("travel.getRiderId", travel.getRiderId() + "");
        Log.e("travel.getRider", travel.getRider() + "");

        getRiderInformation();
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        initViews();

        mapFragment.getMapAsync(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        LocationHelper locationHelper = new LocationHelper(this);
        locationHelper.loadGps(mLocationListener);
        binding.slideStart.setOnSlideCompleteListener(slideView -> startTravel(true));
        binding.slideFinish.setOnSlideCompleteListener(slideView -> finishTravel());
        binding.slideCancel.setOnSlideCompleteListener(slideView -> eventBus.post(new ServiceCancelEvent()));
        String separatedCost = CommonUtils.addSaperator(CommonUtils.removeDecimal(travel.getCostBest()));
        String cost = MyTaxiApplication.getCurrency() + " " + separatedCost;
        binding.costText.setText(cost);
    }


    private void initViews() {
        isMarkerRotating = false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setTrafficEnabled(true);
        LatLng dLocation = new LatLng(getIntent().getDoubleExtra("driverLat", -1), getIntent().getDoubleExtra("driverLng", -1));
        currentLocation = dLocation;

        //********** resize marker*************
        currentMarker = googleMap.addMarker(new MarkerOptions()
                .position(dLocation)
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(R.drawable.pin_driver))));
        destinationMarker = googleMap.addMarker(new MarkerOptions()
                .position(travel.getPickupPoint())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_destination)));
        if (travel.getStartTimestamp() != null)
            startTravel(false);
        googleMap.setOnMapLoadedCallback(() -> {
            List<LatLng> locations = new ArrayList<>();
            locations.add(currentMarker.getPosition());
            locations.add(destinationMarker.getPosition());
            MapHelper.centerLatLngsInMap(gMap, locations, true);
            directionToPassengerRouter = new DirectionsJSONParser(gMap, currentMarker.getPosition(), destinationMarker.getPosition());
            directionToPassengerRouter.run();
        });
        float bearing = (float) bearingBetweenLocations(currentMarker.getPosition(), currentLocation);
        rotateMarker(currentMarker, bearing);

        traceMe(currentMarker.getPosition(), destinationMarker.getPosition());

    }

    private String computeTime(int time) {
        if (time == 0)
            return "00:00";
        int sec = time % 60;
        int min = time / 60;
        return String.format(Locale.getDefault(), "%02d:%02d", min, sec);
    }


    private void Timer() {
        Thread th = new Thread(() -> {
            while (!endTravel) {
                runOnUiThread(() -> {
                    travel.setDurationReal(travel.getDurationReal() + 1);
                    if (binding.getRoot().getResources().getBoolean(R.bool.use_miles)) {
//                        binding.distanceText.setText(MyTaxiApplication.getCurrency()+" "+ travel.getDistanceReal() / 1609.344f);
                        binding.distanceText.setText(binding.getRoot().getContext().getString(R.string.unit_distance_miles, travel.getDistanceReal() / 1609.344f));
                    } else
//                        binding.distanceText.setText(MyTaxiApplication.getCurrency()+" "+ (travel.getDistanceReal() / 1000f));
                        binding.distanceText.setText(getString(R.string.unit_distance, (travel.getDistanceReal() / 1000f)));

                    binding.timeText.setText(computeTime(travel.getDurationReal()));

//                    DecimalFormat f = new DecimalFormat("##.00");
//                    String cost = f.format(travel.getCostBest());
//                    Log.e("getCostBest",travel.getCostBest()+"");
                    String cost = MyTaxiApplication.getCurrency() + " " + CommonUtils.addSaperator(CommonUtils.removeDecimal(travel.getCostBest()));
                    binding.costText.setText(cost);
//                    binding.costText.setText(MyTaxiApplication.getCurrency()+" "+ CommonUtils.removeDecimal(travel.getCostBest()));
//                    binding.costText.setText(getString(R.string.unit_money, travel.getCostBest()));
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        th.start();
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            travel.setDistanceReal(travel.getDurationReal() + LocationHelper.distFrom(currentLocation.latitude, currentLocation.longitude, location.getLatitude(), location.getLongitude()));
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            eventBus.post(new LocationChangedEvent(latLng));
            eventBus.post(new SendTravelInfoEvent(travel));
            geoLog.add(new LatLng(location.getLatitude(), location.getLongitude()));
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
//            Toast.makeText(getApplicationContext(), "location updateD", Toast.LENGTH_SHORT).show();
            float bearing = (float) bearingBetweenLocations(currentMarker.getPosition(), currentLocation);
            rotateMarker(currentMarker, bearing);

            currentMarker.setPosition(currentLocation);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentMarker.getPosition(), gMap.getCameraPosition().zoom);
            gMap.animateCamera(cameraUpdate);
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    void startTravel(boolean fromScratch) {
        if (fromScratch)
            eventBus.post(new ServiceStartEvent());
        TransitionManager.beginDelayedTransition((ViewGroup) (binding.getRoot()));
        binding.slideFinish.setVisibility(View.VISIBLE);
        binding.slideCancel.setVisibility(View.GONE);
        binding.slideStart.setVisibility(View.GONE);
        TransitionManager.beginDelayedTransition(binding.layoutActions);
        binding.inLocationButton.setVisibility(View.GONE);
        binding.callButton.setVisibility(View.GONE);
        destinationMarker.setPosition(travel.getDestinationPoint());
        if (fromScratch) {
            List<LatLng> locations = new ArrayList<>();
            locations.add(currentMarker.getPosition());
            locations.add(destinationMarker.getPosition());
            MapHelper.centerLatLngsInMap(gMap, locations, true);
            if (directionToPassengerRouter != null)
                directionToPassengerRouter.removeLine();
            DirectionsJSONParser directionsJSONParser = new DirectionsJSONParser(gMap, currentMarker.getPosition(), destinationMarker.getPosition());
            directionsJSONParser.run();
        }
        Timer();
        traceMe(currentMarker.getPosition(), destinationMarker.getPosition());
    }

    void finishTravel() {
        if (getResources().getBoolean(R.bool.use_custom_fee)) {
            new MaterialDialog.Builder(this)
                    .title(R.string.travel_fee_dialog_title)
                    .content(R.string.travel_fee_dialog_content)
                    .inputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER)
                    .input(getString(R.string.travel_fee_dialog_hint), "", (dialog, input) -> finishTravel(Double.valueOf(input.toString()))).show();
        } else {
            finishTravel(travel.getCostBest());
        }
    }

    void finishTravel(Double cost) {
        endTravel = true;
        String encodedPoly = "";
        if (geoLog.size() > 0)
            encodedPoly = PolyUtil.encode(PolyUtil.simplify(geoLog, 10));
        eventBus.post(new ServiceFinishEvent(cost, travel.getDurationReal(), travel.getDistanceReal(), encodedPoly));
    }

    public void onInLocationButtonClicked(View v) {
        eventBus.post(new ServiceInLocationEvent());
        TransitionManager.beginDelayedTransition(binding.layoutActions);
        binding.inLocationButton.setVisibility(View.GONE);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCallRequested(ServiceCallRequestResultEvent event) {
        LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    onCallDriverClicked(null);
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.call_request_sent));
    }

    PermissionListener callPermissionListener = new PermissionListener() {
        @SuppressLint("MissingPermission")
        @Override
        public void onPermissionGranted() {
            Intent intent = new Intent(Intent.ACTION_CALL);
            Log.e("travel.getRider", travel.getRider() + "");
            Log.e("mobileNumberIs", travel.getRider() + "");
            intent.setData(Uri.parse("tel:+" + travel.getRider().getMobileNumber() + ""));
            startActivity(intent);
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {

        }

    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceCanceled(ServiceCancelResultEvent event) {
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    eventBus.post(new ServiceCancelEvent());
            });
            return;
        }
        AlertDialogBuilder.show(TravelActivity.this, getString(R.string.service_canceled), AlertDialogBuilder.DialogButton.OK, result -> finish());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServicedFinished(ServiceFinishResultEvent event) {
        //LoadingDialog.dismiss();
        if (event.hasError()) {
            event.showError(TravelActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    finishTravel();
            });
            return;
        }

        sendFinishSMS();

        if (event.isCreditUsed) {

            new MaterialDialog.Builder(this)
                    .title(R.string.message_default_title)
                    .content(R.string.service_finished_credit)
                    .positiveText(R.string.alert_ok)
                    .onPositive((dialog, which) -> finish())
                    .show();
        } else {
            new MaterialDialog.Builder(this)
                    .title(R.string.message_default_title)
                    .content(R.string.service_finished_cash)
                    .positiveText(R.string.alert_ok)
                    .onPositive((dialog, which) -> finish())
                    .show();
        }
    }

    public void onCallDriverClicked(View view) {
        boolean isCallRequestEnabled = getResources().getBoolean(R.bool.is_call_request_enabled_driver);
        boolean isDirectCallEnabled = getResources().getBoolean(R.bool.is_direct_call_enabled_driver);
        if (isCallRequestEnabled && !isDirectCallEnabled)
            eventBus.post(new ServiceCallRequestEvent());
        if (!isCallRequestEnabled && isDirectCallEnabled)
            TedPermission.with(this)
                    .setPermissionListener(callPermissionListener)
                    .setDeniedMessage(R.string.message_permission_denied)
                    .setPermissions(Manifest.permission.CALL_PHONE)
                    .check();
        new MaterialDialog.Builder(this)
                .title(R.string.select_contact_approach)
                .items(new String[]{getString(R.string.direct_call), getString(R.string.operator_call)})
                .itemsCallback((dialog, view1, which, text) -> {
                    if (which == 0)
                        TedPermission.with(TravelActivity.this)
                                .setPermissionListener(callPermissionListener)
                                .setDeniedMessage(R.string.message_permission_denied)
                                .setPermissions(Manifest.permission.CALL_PHONE)
                                .check();
                    if (which == 1)
                        eventBus.post(new ServiceCallRequestEvent());
                })
                .show();
    }


//    ProgressDialog PD;
    private ArrayList<LatLng> traceOfMe = null;
    private Polyline mPolyline = null;

    private void traceMe(LatLng srcLatLng, LatLng destLatLng) {
//        PD = new ProgressDialog(TravelActivity.this);
//        PD.setMessage("Loading..");
//        PD.show();

        String srcParam = srcLatLng.latitude + "," + srcLatLng.longitude;
        String destParam = destLatLng.latitude + "," + destLatLng.longitude;

        String modes[] = {"driving", "walking", "bicycling", "transit"};

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=AIzaSyBcyeuQMy-WBeywimvuRhg7Pvil0IJXYmM";
//            Log.e("url",url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        try {

//                                Log.e("responseGoogle",response1);
                            JSONObject response = null;

                            response = new JSONObject(response1);


                            MapDirectionsParser parser = new MapDirectionsParser();
                            List<List<HashMap<String, String>>> routes = parser.parse(response);
                            ArrayList<LatLng> points = null;

                            for (int i = 0; i < routes.size(); i++) {
                                points = new ArrayList<LatLng>();
                                // lineOptions = new PolylineOptions();

                                // Fetching i-th route
                                List<HashMap<String, String>> path = routes.get(i);

                                // Fetching all the points in i-th route
                                for (int j = 0; j < path.size(); j++) {
                                    HashMap<String, String> point = path.get(j);

                                    double lat = Double.parseDouble(point.get("lat"));
                                    double lng = Double.parseDouble(point.get("lng"));
                                    LatLng position = new LatLng(lat, lng);

                                    points.add(position);
                                }
                            }

                            drawPoints(points, gMap);
//                            PD.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }


                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        PD.dismiss();

                    }
                }) {

        };


        RequestQueue requestQueue = Volley.newRequestQueue(TravelActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new com.android.volley.DefaultRetryPolicy(
                10000,
                com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                com.android.volley.DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        traceOfMe = points;
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : traceOfMe) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (gMap != null) {
            mPolyline = gMap.addPolyline(polylineOpt);

        } else {

        }
        if (mPolyline != null)
            mPolyline.setWidth(10);
        if (gMap != null) {

            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentMarker.getPosition(), 18.0f));

        }
//        mFooterLayout.setVisibility(View.GONE);
    }

    private void getRiderInformation() {
        String url = "http://3.130.44.140/app/api/getRiderInfo/" + travel.getRiderId();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,

                response -> {

                    Log.e("response", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String result_code = jsonObject.getString("result_code");
                        if (result_code.equalsIgnoreCase("200")) {
                            JSONObject riderObj = new JSONObject(jsonObject.getString("rider_info"));
                            String mobile_number = riderObj.getString("mobile_number");
                            Log.e("mobile_number", mobile_number);
                            try {
                                Rider rider = new Rider();
                                long mobile = Long.valueOf(mobile_number);
                                rider.setMobileNumber(mobile);
                                travel.setRider(rider);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    Toast.makeText(this, "Network Error. Please try after sometime.", Toast.LENGTH_LONG).show();
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(TravelActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                40000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void sendFinishSMS() {

        //test info
        //String accountSID = "ACdac04ac3380fe0e4a062ec5d4205b5b7";
        //String authToken = "03da9fbebaa8f31d9834c3f2ece8177a";

        //Live info
        String accountSID = "AC1cc1be213ee3072d4d4dfd8b016311bd";
        String authToken = "b8f471fb2bca933e9d016788f9e16bc0";

        String servicePhone = "+12056750848";
        String riderPhone = "+" + travel.getRider().getMobileNumber();
        //String riderPhone = "+8617187842012";
        String msgBody = "Your trip has finished:\n" + "From " + travel.getPickupAddress() + " to " + travel.getDestinationAddress() + "\n";
        msgBody = msgBody + "Fare: TZS " + travel.getCostBest() + "/=" + "\n";
        msgBody = msgBody + "Thanks for using Barafu Ride.";
        //String msgBody = "thisfisjflskd";
        final String body = msgBody;

        Log.d("message====", servicePhone+"---"+riderPhone+"----"+body);
        String url = "https://api.twilio.com/2010-04-01/Accounts/" + accountSID + "/Messages";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,

                response -> {

                    Log.e("response", response);

                },
                error -> {
                    Toast.makeText(this, "Network Error. Please try after sometime.", Toast.LENGTH_LONG).show();
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("From", servicePhone);
                params.put("To", riderPhone);
                params.put("Body", body);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<String, String>();;
                // add headers <key,value>
                String credentials = accountSID+":"+authToken;
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(),
                        Base64.NO_WRAP);
                headers.put("Authorization", auth);
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(TravelActivity.this);
        requestQueue.add(stringRequest);

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                40000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



}
