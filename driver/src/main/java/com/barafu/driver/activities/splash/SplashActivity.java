package com.barafu.driver.activities.splash;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.databinding.DataBindingUtil;

import com.barafu.common.MyTaxiApplication;
import com.barafu.common.activities.login.LoginActivity;
import com.barafu.common.components.BaseActivity;
import com.barafu.common.events.BackgroundServiceStartedEvent;
import com.barafu.common.events.ConnectEvent;
import com.barafu.common.events.ConnectResultEvent;
import com.barafu.common.events.LoginEvent;
import com.barafu.common.models.Driver;
import com.barafu.common.utils.AlertDialogBuilder;
import com.barafu.common.utils.AlerterHelper;
import com.barafu.common.utils.CommonUtils;
import com.barafu.common.utils.ConnectivityReceiver;
import com.barafu.common.utils.MyPreferenceManager;
import com.barafu.driver.BuildConfig;
import com.barafu.driver.DriverEventBusIndex;
import com.barafu.driver.R;
import com.barafu.driver.activities.main.MainActivity;
import com.barafu.driver.activities.version_checker.VersionUpdateActivity;
import com.barafu.driver.databinding.ActivitySplashBinding;
import com.barafu.driver.events.LoginResultEvent;
import com.barafu.driver.services.DriverService;
import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends BaseActivity {
    MyPreferenceManager SP;
    ActivitySplashBinding binding;
    int RC_SIGN_IN = 123;
    String lastPhoneNumber = "";
    boolean startRequested = false;

    private PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
        }
        @Override
        public void onPermissionGranted() {
            try {
                if (!isMyServiceRunning(DriverService.class))
                    startService(new Intent(SplashActivity.this, DriverService.class));
            } catch (Exception c) {
                c.printStackTrace();
            }
        }
    };
    private View.OnClickListener onLoginClicked = v -> {
        String resourceName = "testMode";
        int testExists = SplashActivity.this.getResources().getIdentifier(resourceName, "string", SplashActivity.this.getPackageName());
        if (testExists > 0) {
            tryLogin(getString(testExists));
            return;
        }
        if (getResources().getBoolean(R.bool.use_custom_login)) {
            startActivityForResult(new Intent(SplashActivity.this, LoginActivity.class), RC_SIGN_IN);
        } else
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build()))
                            .setTheme(getCurrentTheme())
                            .build(),
                    RC_SIGN_IN);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setImmersive(true);
        showConnectionDialog = false;
        try {
            EventBus.builder().addIndex(new DriverEventBusIndex()).installDefaultEventBus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        if (!getString(R.string.fabric_key).equals("")) {
            Fabric.with(this, new Crashlytics());
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        showNetworkAlertDialog();
    }


    private void showNetworkAlertDialog() {
        if (!ConnectivityReceiver.isConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage("There seems to be an issue conecting to internet.\nPlease make sure you're connected to the internet.");
            builder.setPositiveButton("OK", (dialog, id) -> {
                dialog.dismiss();
                if (ConnectivityReceiver.isConnected()) {
                    showNetworkAlertDialog();
                }
            });
            builder.setNegativeButton("NO", (dialog, id) -> {
                dialog.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                finish();
            });
            AlertDialog alert = builder.create();
            alert.setCancelable(false);
            alert.show();
        } else
            checkVersion();
    }
    private void checkVersion() {
        String version_name = com.barafu.common.BuildConfig.VERSION_NAME;
        VersionChecker versionChecker = new VersionChecker();
        try {
            String latestVersion = versionChecker.execute().get();
            Log.e("latestVersion", latestVersion + " ---- " + version_name);
            if (version_name.equals(latestVersion)) {
                setValues();
            } else {
                if (getUpdateCount() == 1) {
                    startActivity(new Intent(this, VersionUpdateActivity.class));
                    finish();

                } else if (getUpdateCount() == 5) {
                    setUpdateCount(0);
                    setValues();
                }
                else
                    setValues();

            }
            setUpdateCount(getUpdateCount() + 1);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
    public class VersionChecker extends AsyncTask<String, String, String> {

        String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

    }

    private void setValues() {
        binding.loginButton.setOnClickListener(onLoginClicked);
        SP = MyPreferenceManager.getInstance(getApplicationContext());
        checkPermissions();

        setLanguageSpinner();
        setCurrencyAdapter();

    }

    private void setCurrencyAdapter() {
        Spinner currency = findViewById(R.id.spCurrencyDriver);
        List<String> list = new ArrayList<>();
//        list.add(getString(R.string.usd));
        list.add(getString(R.string.tzs));
//        list.add(getString(R.string.ksh));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        currency.setAdapter(dataAdapter);

        if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.usd)))
            currency.setSelection(0);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.tzs)))
            currency.setSelection(1);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.ksh)))
            currency.setSelection(2);

        currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (++currencyCount > 1) {
                    if (selectedItem.equals(getString(R.string.usd))) {
                        MyTaxiApplication.setCurrency(getString(R.string.usd));
                        showCurrencyChangedDialog();
                    } else if (selectedItem.equals(getString(R.string.tzs))) {
                        MyTaxiApplication.setCurrency(getString(R.string.tzs));
                        showCurrencyChangedDialog();
                    } else if (selectedItem.equals(getString(R.string.ksh))) {
                        MyTaxiApplication.setCurrency(getString(R.string.ksh));
                        showCurrencyChangedDialog();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    int count = 0;
    int currencyCount = 0;

    public void setLanguageSpinner() {
        Spinner spLanguageDriver = findViewById(R.id.spLanguageDriver);
        List<String> list = new ArrayList<>();
        list.add(getString(R.string.english));
        list.add(getString(R.string.spanish));
//        list.add(getString(R.string.french));
//        list.add(getString(R.string.swahili));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLanguageDriver.setAdapter(dataAdapter);
        if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.english)))
            spLanguageDriver.setSelection(0);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.spanish)))
            spLanguageDriver.setSelection(1);
        else if (getLanguageCurrent().equalsIgnoreCase(getString(R.string.french)))
            spLanguageDriver.setSelection(2);

        spLanguageDriver.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (++count > 1) {
                    if (selectedItem.equals(getString(R.string.english))) {
                        setLanguage("en_US");
                        setLanguageCurrent(getString(R.string.english));
                        showLanguagechangedMessage();
                    } else if (selectedItem.equals(getString(R.string.spanish))) {
                        setLanguage("es");
                        setLanguageCurrent(getString(R.string.spanish));
                        showLanguagechangedMessage();
                    } else if (selectedItem.equals(getString(R.string.french))) {
                        setLanguage("fr");
                        setLanguageCurrent(getString(R.string.french));
                        showLanguagechangedMessage();
                    }
//                    else if (selectedItem.equals(getString(R.string.swahili))) {
//                        setLanguage("sw");
//                        setLanguageCurrent(getString(R.string.swahili));
//                        showLanguagechangedMessage();
//                    }
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showLanguagechangedMessage() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Change Language")
                .setMessage("Language changed Successfully")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                })
                .show();

//        TextView textView = alertDialog.findViewById(android.R.id.message);
//        textView.setTextColor(ContextCompat.getColor(this, R.color.accent_white));

    }
    public void showCurrencyChangedDialog() {
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Currency")
                .setMessage("Currency changed Successfully")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();

//        TextView textView = alertDialog.findViewById(android.R.id.message);
//        textView.setTextColor(ContextCompat.getColor(this, R.color.accent_white));

    }

    private void checkPermissions() {
        if (!CommonUtils.isGPSEnabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_enable_gps), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        if (CommonUtils.isInternetDisabled(this)) {
            AlertDialogBuilder.show(this, getString(R.string.message_internet_connection), AlertDialogBuilder.DialogButton.CANCEL_RETRY, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY) {
                    checkPermissions();
                } else {
                    finishAffinity();
                }
            });
            return;
        }
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(getString(R.string.message_permission_denied))
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    public void tryConnect() {
        try {
            String token = SP.getString("driver_token", null);
            if (token != null && !token.isEmpty()) {
                eventBus.post(new ConnectEvent(token));
                goToLoadingMode();
            } else {
                goToLoginMode();
            }
        } catch (Exception c) {
            c.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectedResult(ConnectResultEvent event) {
        if (event.hasError()) {
            goToLoginMode();
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    tryConnect();
            });
            return;
        }

        CommonUtils.driver = new Gson().fromJson(SP.getString("driver_user", "{}"), Driver.class);
        startMainActivity();
    }

    @Subscribe
    public void onServiceStarted(BackgroundServiceStartedEvent event) {
        tryConnect();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginResultEvent(LoginResultEvent event) {
        if (event.hasError()) {
            event.showError(SplashActivity.this, result -> {
                if (result == AlertDialogBuilder.DialogResult.RETRY)
                    tryLogin(lastPhoneNumber);
                else
                    finish();
            });
            return;
        }
        CommonUtils.driver = event.driver;
        SP.putString("driver_user", event.driverJson);
        SP.putString("driver_token", event.jwtToken);
        tryConnect();

    }

    @Override
    protected void onResume() {
        super.onResume();
        tryConnect();
    }

    private void startMainActivity() {
        if(startRequested)
            return;
        startRequested = true;
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void tryLogin(String phone) {
        lastPhoneNumber = phone;
        goToLoadingMode();
        if (phone.substring(0, 1).equals("+"))
            phone = phone.substring(1);
        eventBus.post(new LoginEvent(Long.valueOf(phone), BuildConfig.VERSION_CODE));
    }

    private void goToLoadingMode() {
        binding.llLanguageLayoutDriver.setVisibility(View.GONE);
        binding.loginButton.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    private void goToLoginMode() {
        binding.llLanguageLayoutDriver.setVisibility(View.VISIBLE);
        binding.loginButton.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                if (getResources().getBoolean(R.bool.use_custom_login)) {
                    tryLogin(data.getStringExtra("mobile"));
                } else {
                    IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
                    String phone;
                    if (idpResponse != null) {
                        phone = idpResponse.getPhoneNumber();
                        tryLogin(phone);
                    }
                }
                return;
            }
            AlerterHelper.showError(SplashActivity.this, getString(R.string.login_failed));
            goToLoginMode();
        }
    }
}
