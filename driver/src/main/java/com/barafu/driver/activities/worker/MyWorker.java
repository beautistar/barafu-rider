package com.barafu.driver.activities.worker;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class MyWorker extends Worker {
    private Context context;
    public static final String KEY_TASK_OUTPUT = "key_task_output";

    public MyWorker(Context context, WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;

    }

    @Override
    public ListenableWorker.Result doWork() {

        Log.e("Result.success()", Result.success() + "");
        Intent intent = new Intent();
        intent.setAction("customreceiveraction");
        intent.putExtra("action", "update");
        context.sendBroadcast(intent);

        return Result.success();
    }





}
